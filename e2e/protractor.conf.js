// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

exports.config = {
  SELENIUM_PROMISE_MANAGER: false,
  allScriptsTimeout: 50000,
  getPageTimeout: 50000,
  specs: ['./src/features/**/*.feature'],
  capabilities: {
    'browserName': 'chrome'
  },
  directConnect: true,
  baseUrl: 'http://localhost:4200/',
  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  cucumberOpts: {
    require: [
      './src/steps/**/*.steps.ts'
    ]
    //  tags: "@debug"
  },
  onPrepare() {
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.e2e.json')
    });

    return new Promise((resolve, reject) => {

      var MailListener = require("mail-listener2");

      var mailListener = new MailListener({
        username: "jonathan.valencia@grupokonecta.com",
        password: "87123hjbasd7",
        host: "172.102.100.31",
        port: 80,
        tls: true,
        tlsOptions: { rejectUnauthorized: false },
        mailbox: "INBOX"
      });

      mailListener.start();

      mailListener.on("server:connected", function () {
        resolve();
      });

      mailListener.on("server:disconnected", function () {
        // Something
      });

      mailListener.on("error", function (err) {
        reject(err);
      });

      global.mailListener = mailListener;
    });
  },
  onCleanUp: function () {
    mailListener.stop();
  }
};