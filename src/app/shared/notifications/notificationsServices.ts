import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
    providedIn: 'root'
  })
export class NotificationsService {

    constructor(private notification: ToastrService) { }

    /**
     * @date (04-07-2020)
     * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
     * @description Metodo encargado de realizar el despliegue de notificaciones.
     * @params { messageTitle, messageText, messageType } data con el cuerpo y contenido de la alerta
    **/
    alert(messageTitle: string, messageText: string, messageType: string) {
        this.notification.clear();
        switch (messageType) {
            case 'error':
                this.notification.error(messageText, messageTitle);
                break;
            case 'info':
                this.notification.info(messageText, messageTitle);
                break;
            case 'success':
                this.notification.success(messageText, messageTitle);
                break;
            case 'warning':
                this.notification.warning(messageText, messageTitle);
                break;
            default:
                this.notification.info(messageText, messageTitle);
                break;
        }
    }
}