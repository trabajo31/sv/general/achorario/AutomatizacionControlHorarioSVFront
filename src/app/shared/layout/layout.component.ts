import { Component, OnInit, ChangeDetectorRef, OnDestroy, AfterViewInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { Subscription } from 'rxjs';
import { AuthenticationService } from './../../core/services/auth.service';
import { SpinnerService } from '../../core/services/spinner.service';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import * as jwt_decode from 'jwt-decode'
import { PermitsService } from 'src/app/services/permits/permits.service';
import { Modules, NameModules } from '../../model/permits.model';
import { NotificationsService } from '../notifications/notificationsServices';
import { MESSAGE_ALERT } from '../../global/globalConfig';
import { AuthInterceptorService } from 'src/app/services/auth-interceptor/auth-interceptor.service';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit, OnDestroy, AfterViewInit {

    private _mobileQueryListener: () => void;
    mobileQuery: MediaQueryList;
    showSpinner: boolean;
    userName: string;
    isAdmin: boolean;
    permissions: any;
    message = MESSAGE_ALERT;
    nameModules = NameModules;
    modules = Modules;
    private autoLogoutSubscription: Subscription;
    data: any;

    constructor(private changeDetectorRef: ChangeDetectorRef,
        private media: MediaMatcher,
        public spinnerService: SpinnerService,
        private authService: AuthenticationService,
        private authGuard: AuthGuard,
        private permitsService: PermitsService,
        private authInterceptorService: AuthInterceptorService,
        private notificationsService: NotificationsService) {

        this.mobileQuery = this.media.matchMedia('(max-width: 1000px)');
        this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        this.mobileQuery.addEventListener("change", this._mobileQueryListener);
    }

    ngOnInit(): void {
        this.getPermitsAlls();
       
        const user = this.authService.getCurrentUser();
        this.isAdmin = user.isAdmin;
        this.userName = user.fullName;
        const timer = TimerObservable.create(2000, 5000);
        this.autoLogoutSubscription = timer.subscribe(t => {
        this.authGuard.canActivate();
        });
    }

    ngOnDestroy(): void {
        this.mobileQuery.removeEventListener("change", this._mobileQueryListener);
        this.autoLogoutSubscription.unsubscribe();
    }

    ngAfterViewInit(): void {
        this.changeDetectorRef.detectChanges();
    }

    /**
    * @date(01-10-2020)
    * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
    * @description Metodo encargado de obtener objeto con modulos y permisos para mostrar los botones del menu
    **/
    getPermitsAlls() {
        let arrayData: any[] = [];
        const user = this.authService.getCurrentUser();
        let token = null;
        if (user) {
          token = user.token;
        }
        if (token != null) {
            let objectTokenJwt = jwt_decode(token);
            let objectUser: any = JSON.parse(objectTokenJwt.sub);
            let objectRol = objectUser.k_rol_user;
            this.permitsService.getPermits(objectRol.pkRol).subscribe(resp => {
                if (resp.status && resp.data != null) {
                    this.walkObjectAlls(resp.data);
                }
            }, error => {
                this.notificationsService.alert('Aviso', this.message.error, 'error');
            });
        }
    }

    /**
    * @date(01-10-2020)
    * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
    * @description Metodo encargado de construir el objeto encargado de mostrar el menu con sus botones
    **/
    walkObjectAlls(data: any) {
        let countData: number = 0;
        for (let index = 0; index < data.length; index++) {
            let nameModule = data[index].fkModule.uuid;
            switch (nameModule) {
                case this.nameModules.countries:
                    this.modules.countries.module = nameModule;
                    this.modules.countries.permission = data[index].fkPermission.permissionCode;
                    this.modules.countries.visible = true;
                    countData++;
                    break;
                case this.nameModules.customers:
                    this.modules.customers.module = nameModule;
                    this.modules.customers.permission = data[index].fkPermission.permissionCode;
                    this.modules.customers.visible = true;
                    break;
                case this.nameModules.holydayByCountry:
                    this.modules.holydayByCountry.module = nameModule;
                    this.modules.holydayByCountry.permission = data[index].fkPermission.permissionCode;
                    this.modules.holydayByCountry.visible = true;
                    countData++;
                    break;
                case this.nameModules.holydays:
                    this.modules.holydays.module = nameModule;
                    this.modules.holydays.permission = data[index].fkPermission.permissionCode;
                    this.modules.holydays.visible = true;
                    countData++;
                    break;
                case this.nameModules.rol:
                    this.modules.rol.module = nameModule;
                    this.modules.rol.permission = data[index].fkPermission.permissionCode;
                    this.modules.rol.visible = true;
                    countData++;
                    break;
                case this.nameModules.service:
                    this.modules.service.module = nameModule;
                    this.modules.service.permission = data[index].fkPermission.permissionCode;
                    this.modules.service.visible = true;
                    countData++;
                    break;
                case this.nameModules.shelude:
                    this.modules.shelude.module = nameModule;
                    this.modules.shelude.permission = data[index].fkPermission.permissionCode;
                    this.modules.shelude.visible = true;
                    countData++;
                    break;
                case this.nameModules.specialShedule:
                    this.modules.specialShedule.module = nameModule;
                    this.modules.specialShedule.permission = data[index].fkPermission.permissionCode;
                    this.modules.specialShedule.visible = true;
                    countData++;
                    break;
                case this.nameModules.moduleByRol:
                    this.modules.moduleByRol.module = nameModule;
                    this.modules.moduleByRol.permission = data[index].fkPermission.permissionCode;
                    this.modules.moduleByRol.visible = true;
                    countData++;
                    break;
                case this.nameModules.user:
                    this.modules.user.module = nameModule;
                    this.modules.user.permission = data[index].fkPermission.permissionCode;
                    this.modules.user.visible = true;
                    countData++;
                    break;
                default:
                    break;
            }
        }
        if (countData === 0) {
            this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'error');
        }
    }
    
    ExitSession(){this.authService.logout();}
}
