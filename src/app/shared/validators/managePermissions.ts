import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import * as jwt_decode from "jwt-decode";
import { PermitsService } from "../../services/permits/permits.service";
import { NotificationsService } from "../notifications/notificationsServices";
import { PERMISSION_MESSAGES } from "../../global/globalConfig";
import { AuthenticationService } from "../../core/services/auth.service";

@Injectable({
  providedIn: "root"
})
export class ManagePermissions {
  // private nameModules: any;
  private message = PERMISSION_MESSAGES;
  //Datos con modulos y permisos
  modulePermissions = { module: "", permission: "", visible: false };

  constructor(
    private permitsService: PermitsService,
    private router: Router,
    private notificationsService: NotificationsService,
    private authenticationService: AuthenticationService,
    private authService: AuthenticationService
  ) {}

  /**
   * @date(01-10-2020)
   * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
   * @description Metodo encargado de obtener un objeto con los permisos del modulo
   * @returns objeto con modulo y sus permisos
   **/
  getModulePermissions() {
    return this.modulePermissions;
  }

  /**
   * @date(01-10-2020)
   * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
   * @description Metodo encargado de obtener un objeto con los permisos del modulo
   * @param nameModule nombre de modulo a obtener con sus permisos
   **/
  getPermitsIndividual(nameModule: string) {
    const arrayData: any[] = [];
    let token = null;
    const user = this.authenticationService.getCurrentUser();
    if (user) {
      token = user.token;
    }
    if (token != null) {
      const objectTokenJwt = jwt_decode(token);
      const objectUser = JSON.parse(objectTokenJwt.sub);
      const objectRol = objectUser.k_rol_user;
      this.permitsService.getPermits(objectRol.pkRol).subscribe(
        (resp) => {
          if (resp.status && resp.data != null) {
            this.walkObjectIndividual(resp.data, nameModule);
          } else {
            this.notificationsService.alert(
              "Aviso",
              this.message.not_get_permissions,
              "error"
            );
            this.authService.logout();
          }
        },
        () => {
          this.notificationsService.alert(
            "Aviso",
            this.message.permissions_error,
            "error"
          );
          this.authService.logout();          
        }
      );
    }
  }

  /**
   * @date(01-10-2020)
   * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
   * @description Metodo encargado de contruir el objeto con su modulo y permisos
   * @param data Datos a recorrer
   * @param name Nombre del modulo
   **/
  walkObjectIndividual(data: any, name: string) {
    for (let index = 0; index < data.length; index++) {
      const nameModule = data[index].fkModule.uuid;
      if (nameModule == name) {
        this.modulePermissions.module = nameModule;
        this.modulePermissions.permission =
          data[index].fkPermission.permissionCode;
        this.modulePermissions.visible = true;
        break;
      }
    }
  }
}
