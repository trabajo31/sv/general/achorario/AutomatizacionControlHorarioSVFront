import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './core/guards/auth.guard';

const appRoutes: Routes = [
    {
        path: 'auth',
        loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
        canActivate: []
    },
    {
        path: 'customers',
        loadChildren: () => import('./pages/customers/customers.module').then(m => m.CustomersModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'service',
        loadChildren: () => import('./pages/service/service.module').then(m => m.ServiceModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'holydays',
        loadChildren: () => import('./pages/holydays/holydays.module').then(m => m.HolydaysModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'rol',
        loadChildren: () => import('./pages/rol/rol.module').then(m => m.RolModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'modulesByRol',
        loadChildren: () => import('./pages/modules-by-roles/modules-by-roles.module').then(m => m.ModulesByRolesModule),
        canActivate: [AuthGuard]
    },
    {

        path: 'specialSchedule',
        loadChildren: () => import('./pages/specialSchedule/specialSchedule.module').then(m => m.SpecialScheduleModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'countries',
        loadChildren: () => import('./pages/countries/countries.module').then(m => m.CountriesModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'holydaysByCountry',
        loadChildren: () => import('./pages/holyday-by-country/holyday-by-country.module').then(m => m.HolydayByCountryModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'user',
        loadChildren: () => import('./pages/user/user.module').then(m => m.UserModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'shedule',
        loadChildren: () => import('./pages/shedule/shedule.module').then(m => m.SheduleModule),
        canActivate: [AuthGuard]
    },
    {
        path: '**',
        redirectTo: 'auth',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes,
        {
            useHash: true,
            onSameUrlNavigation: 'reload',
        })
    ],
    exports: [RouterModule],
    providers: []
})
export class AppRoutingModule { }
