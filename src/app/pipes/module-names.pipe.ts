import { Pipe, PipeTransform } from '@angular/core';
import {NameModules} from 'src/app/model/permits.model';

@Pipe({
  name: 'moduleNames'
})
export class ModuleNamesPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    switch (value) {
      case NameModules.holydayByCountry:
        return 'Festivos por país';
      case NameModules.countries:
        return 'Paises';
      case NameModules.customers:
        return 'Clientes';
      case NameModules.holydays:
        return 'Festivos';
      case NameModules.rol:
        return 'Roles';
      case NameModules.service:
        return 'Servicios';
      case NameModules.shelude:
        return 'Configuración horario';
      case NameModules.specialShedule:
        return 'Horario especial';
      case NameModules.user:
        return 'Usuarios';
      case NameModules.moduleByRol:
        return 'Modulos por Rol';
      default:
        return value;
    }
  }

}
