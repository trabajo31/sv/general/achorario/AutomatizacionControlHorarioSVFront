import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalizePermission'
})
export class CapitalizePermissionPipe implements PipeTransform {

  transform(value: any): any {
    if (value === 'LECTURA Y ESCRITURA') {
      return 'Lectura y Escritura';
    }
    return 'Lectura';
  }

}
