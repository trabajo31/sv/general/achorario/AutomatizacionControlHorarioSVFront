import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { HolydayByCountryService } from 'src/app/services/holyday-by-country/holyday-by-country.service';
import { BreakpointObserver } from '@angular/cdk/layout';
import { GlobalMethods } from 'src/app/global/globalmethods';
import { NotificationsService } from 'src/app/shared/notifications/notificationsServices';
import { CreateHolydayByCountryComponent } from '../modals/create-holyday-by-country/create-holyday-by-country.component';
import { DatePipe } from '@angular/common';
import { ManagePermissions } from 'src/app/shared/validators/managePermissions';
import { NameModules } from '../../../model/permits.model';
import { MESSAGE_ALERT } from '../../../global/globalConfig';

@Component({
  selector: 'app-holyday-by-country-list',
  templateUrl: './holyday-by-country-list.component.html',
  styleUrls: ['./holyday-by-country-list.component.css']
})
export class HolydayByCountryListComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  preloader:boolean;
  public validateFilters: UntypedFormGroup;
  dataPermission: any;
  nameModules = NameModules;

  displayedColumns = ['holydayDate', 'holydayDescription', 'country', 'status', 'action'];

  dataSource: MatTableDataSource<any>;

  private objectDataDelete = {
    id: 0,
    name: '',
    state: 0,
    uid: 0,
  };

  //Mensajes de alerta
  public message = MESSAGE_ALERT;

  constructor(
    private breakPointObserver: BreakpointObserver,
    private holydayByCountryService: HolydayByCountryService,
    private formBuilder: UntypedFormBuilder,
    private globalMethods: GlobalMethods,
    private notificationsService: NotificationsService,
    private datePipe: DatePipe,
    private dialog: MatDialog,
    private managePermissions: ManagePermissions
  ) {
    const columns = ['holydayDate', 'holydayDescription', 'country', 'status', 'action'];
    this.breakPointObserver.observe(['(max-width: 600px)'])
      .subscribe(result => this.displayedColumns = columns);
  }

  ngOnInit() {
    this.initForm();
    this.getData();
    this.managePermissions.getPermitsIndividual(NameModules.holydayByCountry);
  }

  initForm() {
    this.validateFilters = this.formBuilder.group({
      search: ['', [Validators.required, Validators.pattern('[a-zA-ZÑáéíóú ]*')]]
    });
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para obtener la data en la datasource de la tabla
 **/
  getData() {
    this.preloader =true;
    this.holydayByCountryService.getAllHolydaysByCountry().subscribe(resp => {
      if (resp.status) {
        this.dataSource = new MatTableDataSource(resp.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.changeFilterConditions();
      }
      this.preloader = false
    });
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para abrir la modal de editar o agregar un nuevo país
 **/
  openModal(dataModal = null) {
    this.dataPermission = this.managePermissions.getModulePermissions();
    if (this.dataPermission.permission === 1) {
      this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'warning');
    } else if (this.dataPermission.permission === 2) {
      let dialogRef;
      dialogRef = this.dialog.open(CreateHolydayByCountryComponent, {
        data: dataModal,
        height: '250px',
        width: '600px',
        autoFocus: false,
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result === 'acept') {
          this.ngOnInit();
        }
      });
    } else {
      this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'warning');
    }
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para cambiar la forma de filtrar la busqueda
 **/
  changeFilterConditions() {
    this.dataSource.filterPredicate = (data, filter) => {
      if (data.k_country.countryName.toLowerCase().indexOf(filter) >= 0) {
        return true;
      }
      if (data.k_holyday.descriptionHolyday.toLowerCase().indexOf(filter) >= 0) {
        return true;
      }
      const date = this.datePipe.transform(data.k_holyday.dateHolyday, 'dd/MM/yyyy').toString();
      if (date.indexOf(filter) >= 0) {
        return true;
      }
      return false;
    };
  }


  filterTable(event: Event) {
    const filter = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filter.trim().toLowerCase();
  }

  cleanSearchEngine() {
    if (this.validateFilters.get('search').value !== '') {
      this.validateFilters.get('search').setValue('');
    }
  }

}
