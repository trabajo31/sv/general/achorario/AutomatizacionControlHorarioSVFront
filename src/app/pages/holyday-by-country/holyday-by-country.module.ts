import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { HolydayByCountryRoutingModule } from './holyday-by-country-routing.module';
import { HolydayByCountryListComponent } from './holyday-by-country-list/holyday-by-country-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CreateHolydayByCountryComponent } from './modals/create-holyday-by-country/create-holyday-by-country.component';


@NgModule({
    declarations: [HolydayByCountryListComponent, CreateHolydayByCountryComponent],
    imports: [
        CommonModule,
        SharedModule,
        HolydayByCountryRoutingModule
    ],
    providers: [DatePipe]
})
export class HolydayByCountryModule { }
