import { Component, OnInit, Inject } from '@angular/core';
import { UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { HolydayService } from 'src/app/services/holydays/holyday.service';
import { GlobalMethods } from 'src/app/global/globalmethods';
import { CountriesService } from 'src/app/services/countries/countries.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { HolydayByCountryService } from 'src/app/services/holyday-by-country/holyday-by-country.service';
import { NotificationsService } from 'src/app/shared/notifications/notificationsServices';
import { v4 as uuidv4 } from 'uuid';
@Component({
  selector: 'app-create-holyday-by-country',
  templateUrl: './create-holyday-by-country.component.html',
  styleUrls: ['./create-holyday-by-country.component.css']
})
export class CreateHolydayByCountryComponent implements OnInit {

  // Controller de los campos del formulario(Valores que se muestran)
  public validateFilters: UntypedFormGroup;

  // Variable para almacenar el valor real del objeto que se selecciona
  holyDaysValue: any;

  // opciones para los filtros
  holydaysFilteredOptions: Observable<any[]>;
  holydaysDates: any[];

  countryValue: any;

  countriesFilteredOptions: Observable<any[]>;
  countries: any[] = [];

  public statusActivated: Boolean;
  public titleModal: String;
  public filterOptions;
  public modalUpdate: Boolean;

  private objectDataUpdate = {
    id: 0,
    k_holyday: {},
    k_country: {},
    state: 0
  };

  /**
 * @date(16-07-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Constructor de la clase
 **/
  constructor(
    private datePipe: DatePipe,
    private holydayByCountryService: HolydayByCountryService,
    private holydayService: HolydayService,
    private countryService: CountriesService,
    private globalMethods: GlobalMethods,
    private formBuilder: UntypedFormBuilder,
    private notificationsService: NotificationsService,
    public dialogRef: MatDialogRef<CreateHolydayByCountryComponent>,
    @Inject(MAT_DIALOG_DATA) public dataUpdate: any
  ) {
    this.holydaysDates = [];
    this.countries = [];
    this.statusActivated = false;
    this.filterOptions = this.globalMethods.filterOptions;
    this.getData();
  }


  /**
 * @date(16-07-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Inicializar el componente
 **/
  ngOnInit() {
    this.initForm();
    this.validateTypeModal();
    this.initHolydaysArray();
    this.initCountriesArray();
  }


  /**
 * @date(16-07-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Metodo para inicializar el formulario
 **/
  initForm() {
    this.validateFilters = this.formBuilder.group({
      holyday: ['', Validators.required],
      country: ['', Validators.required],
      state: ['', Validators.required]
    });
  }



  /**
 * @date(16-07-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Validar si el modal es de creación 
 **/
  validateTypeModal() {
    if (this.dataUpdate !== undefined && this.dataUpdate !== null && this.dataUpdate !== '') {
      this.titleModal = 'Actualizar festivo por país';
      this.statusActivated = true;
      this.modalUpdate = true;
      this.preloadDataUpdate();
    } else {
      this.titleModal = 'Agregar nuevo festivo por país';
    }
  }



  /**
 * @date(16-07-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Precargar cuando el formulario sea de edición 
 **/
  preloadDataUpdate() {
    const { k_country, k_holyday } = this.dataUpdate;
    this.countryValue = {
      id: k_country.idCountry,
      uid: k_country.pkCountry,
      name: k_country.countryName,
      state: k_country.countryState
    };
    this.holyDaysValue = {
      idh: k_holyday.idHolyday,
      uidh: k_holyday.pkHolyday,
      descripcionh: k_holyday.descriptionHolyday,
      dateH: k_holyday.dateHolyday,
      stateh: k_holyday.stateHolyday
    };
    this.validateFilters.get('country').setValue(this.countryValue.name);
    this.validateFilters.get('holyday')
      .setValue(this.datePipe.transform(this.holyDaysValue.dateH, 'dd/MM/yyyy').toString());
    this.validateFilters.get('state').setValue(Number(this.dataUpdate.state));
  }


  /**
 * @date(16-07-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Inicializar arreglo de paises
 **/
  initCountriesArray() {
    this.countriesFilteredOptions = this.validateFilters.get('country').valueChanges.pipe(
      startWith(''),
      map(value => {
        if (value.name) {
          this.countryValue = value;
          this.validateFilters.get('country').setValue(value.name);
        }
        return this._filterCountry(value);
      })
    );
  }


  /**
 * @date(16-07-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Inicializar arreglo de holydays
 **/
  initHolydaysArray() {
    this.holydaysFilteredOptions = this.validateFilters.get('holyday').valueChanges.pipe(
      startWith(''),
      map(value => {
        if (value.dateH) {
          this.holyDaysValue = value;
          this.validateFilters.get('holyday')
            .setValue(this.datePipe.transform(this.validateFilters.get('holyday').value.dateH, 'dd/MM/yyyy').toString());
        }
        return this._filterHolydays(value);
      })
    );
  }

  /**
 * @date(16-07-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Filtrar arreglo de paises 
 **/
  private _filterCountry(value: string): string[] {
    value = value.toString();
    const filterValue = value.toLowerCase();
    return this.countries.filter(option => option.name.toLowerCase().indexOf(filterValue) >= 0);
  }

  /**
 * @date(16-07-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description filtrar arreglo de festivos 
 **/
  private _filterHolydays(value): any[] {
    value = value.toString();
    return this.holydaysDates.filter(option => {
      const myDate = this.datePipe.transform(new Date(option.dateH), 'dd/MM/yyyy').toString();
      return myDate.indexOf(value) >= 0;
    });
  }

  /**
 * @date(16-07-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description obtener los datos para llenar las listas de holydays y paises
 **/
  getData() {
    this.holydayService.getAllHolyDays().subscribe(holydays => {
      this.holydaysDates = holydays.data.filter(holyday => holyday.stateh === 1);
      this.initHolydaysArray();
    });
    this.countryService.getAllCountries().subscribe(countries => {
      this.countries = countries.data;
      this.initCountriesArray();
    });
  }

  /**
 * @date(16-07-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Metodo que llama el servicio para guardar o actualizar
 **/
  save() {
    try {
      const countryExists = this.countries.filter(country => country.name === this.validateFilters.get('country').value);

      if (countryExists.length <= 0) {
        this.notificationsService.alert('Aviso', 'Debe seleccionar un país de la lista', 'warning');
        return;
      }

      const dateExists = this.holydaysDates.filter(date => {
        return this.datePipe.transform(date.dateH, 'dd/MM/yyyy').toString() === this.validateFilters.get('holyday').value;
      });

      if (dateExists.length <= 0) {
        this.notificationsService.alert('Aviso', 'Debe seleccionar un festivo de la lista', 'warning');
        return;
      }

      const params = this.objectDataUpdate;
      if (this.checkFilters(this.modalUpdate)) {
        if (this.modalUpdate) {
          params.id = this.dataUpdate.id;
          params.k_country = {
            pkCountry: this.countryValue.uid,
            countryName: this.countryValue.name,
            countryState: this.countryValue.state,
            idCountry: this.countryValue.id
          };
          params.k_holyday = {
            pkHolyday: this.holyDaysValue.uidh,
            descriptionHolyday: this.holyDaysValue.descripcionh,
            stateHolyday: this.holyDaysValue.stateh,
            idHolyday: this.holyDaysValue.idh,
            dateHolyday: this.holyDaysValue.dateH
          };
          params.state = this.validateFilters.get('state').value;
          this.updateHolyDayByCountry(params);
        } else {
          const idData = uuidv4();
          params.id = idData;
          params.k_country = {
            pkCountry: this.countryValue.uid,
            countryName: this.countryValue.name,
            countryState: this.countryValue.state,
            idCountry: this.countryValue.id
          };
          params.k_holyday = {
            pkHolyday: this.holyDaysValue.uidh,
            descriptionHolyday: this.holyDaysValue.descripcionh,
            stateHolyday: this.holyDaysValue.stateh,
            idHolyday: this.holyDaysValue.idh,
            dateHolyday: this.holyDaysValue.dateH
          };
          params.state = 1;
          this.saveHolyDayByCountry(params);
        }
      } else {
        this.notificationsService.alert('Aviso', 'Los campos marcados son obligatorios', 'warning');
      }
    } catch (error) {
      this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'error');
    }

  }

  /**
 * @date(16-07-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Guardar 
 **/
  saveHolyDayByCountry(params) {
    this.holydayByCountryService.save(params).subscribe(resp => {
      if (resp.responseCode === 400) {
        this.notificationsService.alert('Aviso', 'Registro fue previamente realizado', 'warning');
        return;
      }
      this.closemodal();
      this.notificationsService.alert('Aviso', 'Datos almacenados correctamente', 'success');
    }, error => {
      this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'error');
    });
  }

  /**
 * @date(16-07-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Actualizar
 **/
  updateHolyDayByCountry(params) {
    this.holydayByCountryService.update(params).subscribe(resp => {
      this.closemodal();
      this.notificationsService.alert('Aviso', 'Datos almacenados correctamente', 'success');
    }, error => {
      this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'error');
    });
  }

  checkFilters(option: Boolean): Boolean {
    if (option) {
      if (this.validateFilters.valid) {
        return true;
      } else {
        return false;
      }
    } else {
      if (this.validateFilters.get('holyday').valid && this.validateFilters.get('country').valid) {
        return true;
      } else {
        return false;
      }
    }
  }

  closemodal() {
    this.dialogRef.close('acept');
  }

}
