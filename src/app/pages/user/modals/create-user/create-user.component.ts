import { Component, OnInit, Inject } from '@angular/core';
import { UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { NotificationsService } from 'src/app/shared/notifications/notificationsServices';
import { CreateRolComponent } from 'src/app/pages/rol/modals/create-rol/create-rol.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GlobalMethods } from 'src/app/global/globalmethods';
import { UserService } from 'src/app/services/user/user.service';
import { RolService } from 'src/app/services/rol/rol.service';
import { ModelUser } from '../../config/config';
import { ModelRolTblm } from '../../../rol/config/config';
import { v4 as uuidv4 } from 'uuid';
import { MESSAGE_ALERT } from '../../../../global/globalConfig';
import { 
  virtualServiceValidator, validateXSS 
} from '../../../../services/utils/directives/form-validations.directive';



@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  preloader:boolean;
  //Objeto para controlar filtros.
  public validateFilters: UntypedFormGroup;
  //Objetos que controlan la modal.
  public statusActivated: boolean = false;
  //Titulo de modal
  public titleModal: string;
  //Opciones del filtro
  public filterOptions = this.globalMethods.filterOptions;
  //Validar si la actualización esta activa
  public modalUpdate: boolean = false;
  //Objetos con datos
  public dataRol: any;
  //Mensajes de alerta
  public message = MESSAGE_ALERT;
    //Mensajes de alerta
  public clienValidate = 'Nombre';


  constructor(
    private fb: UntypedFormBuilder,
    private notificationsService: NotificationsService,
    public dialogRef: MatDialogRef<CreateRolComponent>,
    private globalMethods: GlobalMethods,
    private rolService: RolService,
    private userService: UserService,
    @Inject(MAT_DIALOG_DATA) public dataUpdate
  ) {
    this.getRoles();
  }

  ngOnInit() {
    this.initForm();
    this.validateTypeModal();
  }

  /**
    * @date(22-02-2020)
    * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
    * @description Metodo encargado de obtener todos los roles actualmente creados.
    **/
  getRoles() {
    this.rolService.getAllService().subscribe(resp => {
      if (resp.status && resp.data != null) {
        this.dataRol = resp.data;
      } else {
        this.notificationsService.alert('Aviso', this.message.error, 'error');
      }
    }, error => {
      this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'error');
    });
  }

  /**
  * @date(24-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de inicializar los filtros.
  **/
  initForm() {
    this.validateFilters = this.fb.group({
      name: ['', [Validators.required, virtualServiceValidator(this.clienValidate), validateXSS()]],
      document: ['', [Validators.required]],
      user: ['', [Validators.required, virtualServiceValidator('usuario'), validateXSS()]],
      rol: ['', Validators.required],
      status: [''],
      creation_date: ['']
    });
  }

  /**
  * @date(22-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de de validar si la modal que abrira sera de  crear o actualziar,
  * al igual que precargara los datos en caso de que sea  actualizar.
  **/
  validateTypeModal() {
    if (this.dataUpdate != undefined && this.dataUpdate != null && this.dataUpdate != '') {
      this.titleModal = "Actualizar Usuario";
      this.statusActivated = true;
      this.modalUpdate = true;
      this.preloadDataUpdate();
    } else {
      this.cleanValidationStatus();
      this.titleModal = "Crear Nuevo Usuario";
    }
  }

  /**
  * @date(22-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de eliminar los validadores del campo status
  **/
  cleanValidationStatus() {
    this.validateFilters.controls['status'].clearValidators();
    this.validateFilters.controls['status'].updateValueAndValidity();
  }

  /**
  * @date(22-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de precargar los datos en la modal de Actualizar.
  **/
  preloadDataUpdate() {
    this.validateFilters.get('name').setValue(this.dataUpdate.name_user);
    this.validateFilters.get('document').setValue(this.dataUpdate.number_id_user);
    this.validateFilters.get('user').setValue(this.dataUpdate.network_user);
    this.validateFilters.get('rol').setValue(this.dataUpdate.k_rol_user.pkRol);
    this.validateFilters.get('status').setValue(this.dataUpdate.status);
    this.validateFilters.get('creation_date').setValue(this.dataUpdate.creation_date);
  }

  /**
  * @date(22-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de guardar los datos.
  **/
  action() {
    if (this.checkFilters()) {
      let modelUser = ModelUser;
      if (this.modalUpdate) {
        if (this.dataUpdate != null && this.dataUpdate != undefined && this.dataUpdate != '') {
          modelUser.pk_user = this.dataUpdate.pk_user;
          modelUser.network_user = this.validateFilters.get('user').value.toUpperCase();
          modelUser.name_user = this.validateFilters.get('name').value.toUpperCase();
          modelUser.id_user = this.dataUpdate.id_user;
          modelUser.number_id_user = this.validateFilters.get('document').value;
          modelUser.k_rol_user = this.transformDataRolToTbl(this.validateFilters.get('rol').value);
          modelUser.status = this.validateFilters.get('status').value;
          modelUser.creation_date = this.validateFilters.get('creation_date').value;
          this.update(modelUser);
        } else {
          this.notificationsService.alert('Aviso', this.message.error, 'warning');
        }
      } else {
        modelUser.pk_user = uuidv4();
        modelUser.network_user = this.validateFilters.get('user').value.toUpperCase();
        modelUser.name_user = this.validateFilters.get('name').value.toUpperCase();
        modelUser.id_user = uuidv4();
        modelUser.number_id_user = this.validateFilters.get('document').value;
        modelUser.k_rol_user = this.transformDataRolToTbl(this.validateFilters.get('rol').value);
        modelUser.status = 1;
        this.save(modelUser);
      }
    } else {
      this.notificationsService.alert('Aviso', this.message.mandatory, 'warning');
    }
  }

  /**
  * @date(22-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de actualizar la data del usuario
  **/
  update(modelUser: any) {
    this.userService.update(modelUser).subscribe(resp => {
      if (resp.status) {
        this.notificationsService.alert('Aviso', this.message.update, 'success');
        this.closemodal();
      }
    }, error => {
      if (error.status === 500) {
        this.notificationsService.alert('Aviso', this.message.duplicate, 'warning');
      } else {
        this.notificationsService.alert('Aviso', this.message.error, 'error');
      }
    });
  }

  /**
  * @date(22-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de guardar la data del  usuario
  **/
  save(modelUser: any) {
    this.preloader = true;
    this.userService.save(modelUser).subscribe(resp => {
      if (resp.status) {
        this.notificationsService.alert('Aviso', this.message.add, 'success');
        this.closemodal();
      }
      this.preloader = false;
    }, error => {
      if (error.status === 500) {
        this.notificationsService.alert('Aviso', this.message.duplicate, 'warning');
      } else {
        this.notificationsService.alert('Aviso', this.message.error, 'error');
      }
    });
  }

  /**
    * @date(22-02-2020)
    * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
    * @description Metodo encargado de transformar el rol TDO seleccionado a un objeto Tblm para El backend.
    **/
  transformDataRolToTbl(idRolTdo: any) {
    let dataFound: any;
    for (let index of this.dataRol) {
      if (index.id === idRolTdo) {
        dataFound = index;
        break;
      }
    }
    let modelRol = ModelRolTblm;
    modelRol.pkRol = dataFound.id;
    modelRol.idRol = dataFound.uid;
    modelRol.descriptionRol = dataFound.name;
    modelRol.stateRol = dataFound.state;
    return modelRol;
  }

  /**
   * @date(24-02-2020)
   * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
   * @description Metodo encargado de verificar si todos los filtros fueron validos.
   **/
  checkFilters(): boolean {
    if (this.validateFilters.valid) {
      return true;
    } else {
      return false;
    }
  }

  /**
    * @date(22-02-2020)
    * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
    * @description Metodo encargado de cerrar la modal.
    **/
  closemodal() {
    this.dialogRef.close('acept');
  }

  /**
  * @date(29-09-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de validar si entran caracteres diferentes a numeros
  **/
  validateNumbers(event) {
    return this.globalMethods.validateOnlyNumbers(event);
  }

  /**
  * @date(29-09-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de validar la longitud de la cadena de texto.
  **/
  validateSize(field: string, value: any, maxSize: number) {
    return this.globalMethods.validateCharacterSize(field, value, maxSize);
  }

  get formValidator() {   
    return this.validateFilters.controls; 
  }

}
