import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModulesByRolesRouting } from './modules-by-roles-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ModulesByRolesListComponent } from './modules-by-roles-list/modules-by-roles-list.component';
import { CreateModuleByRoleComponent } from './modals/create-module-by-role/create-module-by-role.component';
import { ModuleNamesPipe } from '../../pipes/module-names.pipe';
import { CapitalizePermissionPipe } from 'src/app/pipes/capitalize-permission.pipe';




@NgModule({
    declarations: [ModuleNamesPipe, ModulesByRolesListComponent, CreateModuleByRoleComponent, CapitalizePermissionPipe],
    imports: [
        CommonModule,
        ModulesByRolesRouting,
        SharedModule,
    ]
})
export class ModulesByRolesModule { }
