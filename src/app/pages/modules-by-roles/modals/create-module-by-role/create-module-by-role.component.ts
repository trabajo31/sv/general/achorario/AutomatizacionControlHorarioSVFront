import { Component, OnInit, Inject } from '@angular/core';
import { startWith, map } from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';
import { Observable } from 'rxjs';
import { UntypedFormGroup, UntypedFormBuilder, Validators, FormControl } from '@angular/forms';
import { GlobalMethods } from '../../../../global/globalmethods';
import { NotificationsService } from '../../../../shared/notifications/notificationsServices';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RolService } from '../../../../services/rol/rol.service';
import { ModulesByRolesService } from '../../../../services/modules-by-roles/modules-by-roles.service';
import {NameModules} from '../../../../model/permits.model';
import { clientValidValidator } from '../../../../services/utils/directives/form-validations.directive';

@Component({
  selector: 'app-create-module-by-role',
  templateUrl: './create-module-by-role.component.html',
  styleUrls: ['./create-module-by-role.component.css']
})
export class CreateModuleByRoleComponent implements OnInit {

  public validateFilters: UntypedFormGroup;

  public statusActivated: Boolean = false;

  public titleModal: string;

  roles = [];
  rolesName: string[] = [];
  public rolesFilteredOptions: Observable<object[]>;
  rolValue = null;

  modules = [];
  moduleValue = null;
  private modalUpdate: boolean = false;

  permissions = [];
  // private permissionFilterOptions: Observable<any[]>;
  permissionValue = null;
  
  get formValidator() {
    return this.validateFilters.controls; 
  }
  
  constructor(
    private globalMethods: GlobalMethods,
    private formBuilder: UntypedFormBuilder,
    private notificationsService: NotificationsService,
    public dialogRef: MatDialogRef<CreateModuleByRoleComponent>,
    private rolesService: RolService,
    private modulesByRolService: ModulesByRolesService,
    @Inject(MAT_DIALOG_DATA) public dataUpdate
  ) {
    this.modules = [];
    this.permissions = [];
    this.roles = [];
    this.getData();
  }

  ngOnInit() {
    this.initForm();
    this.validateTypeModal();
    this.initRolesArray();
  }

  initForm() {
    this.validateFilters = this.formBuilder.group({
      module: ['', Validators.required],
      permission: ['', Validators.required],
      rol:  new FormControl('', [Validators.required, clientValidValidator(this.rolesName)])
    });
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para validar si es update o un nuevo registro
 **/
  validateTypeModal() {
    if (this.dataUpdate !== undefined && this.dataUpdate !== null && this.dataUpdate !== '' && this.dataUpdate.fkRol) {
      this.titleModal = 'Actualizar permisos por rol';
      this.statusActivated = true;
      this.modalUpdate = true;
      this.preloadDataUpdate();
    } else {
      this.titleModal = 'Agregar nuevo permiso por rol';
    }
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para pre cargar datos en caso de que sea update
 **/
  preloadDataUpdate() {
    const { fkModule, fkPermission, fkRol } = this.dataUpdate;
    this.rolValue = {
      pkRol: fkRol.pkRol,
      descriptionRol: fkRol.descriptionRol,
      stateRol: fkRol.stateRol,
      idRol: fkRol.idRol
    };
    this.moduleValue = fkModule;
    this.permissionValue = fkPermission;

    this.validateFilters.get('module').setValue(this.changeModuleNames(this.moduleValue.moduleName));
    this.validateFilters.get('rol').setValue(this.rolValue.descriptionRol);
    this.validateFilters.get('permission').setValue(this.permissionValue);
  }


  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para inicializar el arreglo de roles
 **/
  initRolesArray() {
    this.rolesFilteredOptions = this.validateFilters.get('rol').valueChanges.pipe(
      startWith(''),
      map(value => {
        if (value.name) {
          this.rolValue = {
            pkRol: value.id,
            descriptionRol: value.name,
            stateRol: value.state,
            idRol: value.uid
          };
          this.validateFilters.get('rol').setValue(value.name);
        }
        return this._filterRol(value);
      })
    );
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para inicializar el arreglo de permisos
 **/
  // initPermissionsArray() {
  //   this.permissionFilterOptions = this.validateFilters.get('permission').valueChanges.pipe(
  //     startWith(''),
  //     map(value => {
  //       if (value.permissionDescription) {
  //         this.permissionValue = value;
  //         this.validateFilters.get('permission').setValue(value.permissionDescription);
  //       }
  //       return this._filterPermission(value);
  //     })
  //   );
  // } 

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para guardar un nuevo registro de permisos por rol
 **/
  save() {
    this.modules.forEach(element => {
      if(element.moduleName == this.validateFilters.get('module').value){
        this.moduleValue = element;
      }
    });
    try {
      if (this.checkFilters(this.modalUpdate)) {
        if (this.modalUpdate) {
          const params = {
            uuid: this.dataUpdate.uuid,
            fkModule: this.moduleValue,
            fkRol: this.rolValue,
            fkPermission: this.validateFilters.get('permission').value
          };
          this.saveModuleByRol(params);
        } else {
          const params = {
            uuid: uuidv4(),
            fkModule: this.moduleValue,
            fkRol: this.rolValue,
            fkPermission: this.validateFilters.get('permission').value
          };
          this.saveModuleByRol(params);
        }

      } else {
        this.notificationsService.alert('Aviso', 'Por favor revise el formulario', 'warning');
      }
    } catch (error) {
      this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'error');
    }
  }

  permissionChange = o1 => {
    return o1.uuid === this.validateFilters.get('permission').value.uuid;
  };
  /**
  * @date(25-09-2020)
  * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
  * @description Funcion para guardar un nuevo registro de permisos por rol
  **/
  saveModuleByRol(params) {
    const dataFilter = this.dataUpdate.dataModulesByRol.filter(data => {
      if (data.fkModule.uuid === params.fkModule.uuid && data.fkRol.pkRol === params.fkRol.pkRol) {
        return true;
      }
      return false;
    });
    if (this.modalUpdate) {
      if (this.dataUpdate.fkRol.pkRol === params.fkRol.pkRol && this.dataUpdate.fkModule.uuid === params.fkModule.uuid
        && this.dataUpdate.fkPermission.uuid === params.fkPermission.uuid) {
        this.notificationsService.alert('Aviso', 'Ya el rol ' + this.dataUpdate.fkRol.descriptionRol + ' cuenta con los permisos para este modulo', 'warning');
        return;
      } else {
        if (dataFilter.length > 0 && this.dataUpdate.fkPermission.uuid === params.fkPermission.uuid) {
          this.notificationsService.alert('Aviso', 'Ya el rol ' + this.dataUpdate.fkRol.descriptionRol + ' cuenta con los permisos para este modulo', 'warning');
          return;
        }
      }
    } else {
      if (dataFilter.length > 0) {
        this.notificationsService.alert('Aviso', 'Registro fue previamente realizado', 'warning');
        return;
      }
    }
    this.modulesByRolService.save(params).subscribe(resp => {
      if (resp.responseCode === 400) {
        this.notificationsService.alert('Aviso', 'Registro fue previamente realizado', 'warning');
        return;
      }
      this.closemodal();
      this.notificationsService.alert('Aviso', this.modalUpdate ? 'Datos Actualizados Correctamente' : 'Datos almacenados correctamente', 'success');
    }, _error => {
      this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'error');
    });
  }

  closemodal() {
    this.dialogRef.close('acept');
  }


  checkFilters(_option: Boolean): Boolean {
    if (this.validateFilters.valid) {
      return true;
    } else {
      return false;
    }
  }

//   /**
//  * @date(25-09-2020)
//  * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
//  * @description Funcion para inicializar el arreglo de modulos
//  **/
//   initModulesArray() {
//     console.log(typeof this.moduleFilterOptions)
//     this.moduleFilterOptions = this.validateFilters.get('module').valueChanges.pipe(
//       startWith(''),
//       map(value => {
//         if (value.moduleName) {
//           this.moduleValue = value;
//           this.validateFilters.get('module').setValue(this.changeModuleNames(value.moduleName));
//         }
//         return this._filterModules(value);
//       })
//     );
//   }



  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para cambiar la manera de filtrar el select de modulos
 **/
  _filterModules(value) {
    value = value.toString();
    const filterValue = value.toLowerCase();
    return this.modules.filter(option => {
      return this.changeModuleNames(option.moduleName).toLowerCase().indexOf(filterValue) >= 0;
    });
  }




  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para cambiar los nombres de los modulos de forma visual
 **/
  changeModuleNames(value) {
    switch (value) {
      case NameModules.holydayByCountry:
        return 'Festivos por país';
      case NameModules.countries:
        return 'Paises';
      case NameModules.customers:
        return 'Clientes';
      case NameModules.holydays:
        return 'Festivos';
      case NameModules.rol:
        return 'Roles';
      case NameModules.service:
        return 'Servicios';
      case NameModules.shelude:
        return 'Configuración horario';
      case NameModules.specialShedule:
        return 'Horario especial';
      case NameModules.user:
        return 'Usuarios';
      case NameModules.moduleByRol:
        return 'Modulos por Rol';
      default:
        return value;
    }
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion filtrar los permisos 
 **/
  _filterPermission(value) {
    value = value.toString();
    const filterValue = value.toLowerCase();
    return this.permissions.filter(option => {
      return option.permissionDescription.toLowerCase().indexOf(filterValue) >= 0;
    });
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para filtrar los roles
 **/
  private _filterRol(value) {
    value = value.toString();
    const filterValue = value.toLowerCase();
    return this.roles.filter(option => {
      return option.name.toLowerCase().indexOf(filterValue) >= 0;
    });
  }


  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para obtener la data
 **/
  getData() {
    this.rolesService.getAllService().subscribe(roles => {
      this.roles = roles.data;
      this.roles.forEach(e => {
        this.rolesName.push(e.name);
      });
      this.initRolesArray();
    });
    this.modulesByRolService.getAllPermissions().subscribe(permission => {
      this.permissions = permission.data;
    });
    this.modulesByRolService.getAllModules().subscribe(modules => {
      this.modules = modules.data;
    });
  }

}
