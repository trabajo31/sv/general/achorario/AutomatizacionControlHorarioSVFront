import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UntypedFormGroup, UntypedFormBuilder } from '@angular/forms';
import { ModulesByRolesService } from '../../../services/modules-by-roles/modules-by-roles.service';
import { BreakpointObserver } from '@angular/cdk/layout';
import { NotificationsService } from '../../../shared/notifications/notificationsServices';
import { GlobalMethods } from '../../../global/globalmethods';
import { CreateModuleByRoleComponent } from '../modals/create-module-by-role/create-module-by-role.component';
import { ManagePermissions } from '../../../shared/validators/managePermissions';
import { NameModules } from '../../../model/permits.model';
import { MESSAGE_ALERT } from '../../../global/globalConfig';
@Component({
  selector: 'app-modules-by-roles-list',
  templateUrl: './modules-by-roles-list.component.html',
  styleUrls: ['./modules-by-roles-list.component.css']
})
export class ModulesByRolesListComponent implements OnInit {

  preloader:boolean;
  // Objetos encargados de paginar y organizar la tabla.
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public validateFilters: UntypedFormGroup;

  displayedColumns = ['rol', 'module', 'permission', 'action'];

  dataSource = new MatTableDataSource<object>();
  dataPermission: any;
  nameModules = NameModules;
  //Mensajes de alerta
  public message = MESSAGE_ALERT;

  dataTable: any;

  constructor(
    private modulesByRolService: ModulesByRolesService,
    breakpointObserver: BreakpointObserver,
    private dialog: MatDialog,
    private notificationsService: NotificationsService,
    private globalMethods: GlobalMethods,
    private formBuilder: UntypedFormBuilder,
    private managePermissions: ManagePermissions
  ) { }

  ngOnInit() {
    this.initForm();
    this.getModulesByRol();
    this.managePermissions.getPermitsIndividual(NameModules.moduleByRol);
  }

  initForm() {
    this.validateFilters = this.formBuilder.group({
      search: ['']
    });
  }

  refreshData() {
    this.cleanSearchEngine();
    this.getModulesByRol();
  }

  
  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Abrir la modal para actualizar o crear un registro nuevo
 **/
  openModal(dataModal = {}) {
    this.dataPermission = this.managePermissions.getModulePermissions();
    dataModal['dataModulesByRol'] = this.dataTable;
    if (this.dataPermission.permission === 1) {
      this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'warning');
    } else if (this.dataPermission.permission === 2) {
      let dialogRef = this.dialog.open(CreateModuleByRoleComponent, {
        data: dataModal,
        // height: '300px',
        width: '600px',
        autoFocus: false,
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result === 'acept') {
          this.ngOnInit();
        }
      });
    } else {
      this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'warning');
    }
  }

  
  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Obtener los datos para llenar la tabla
 **/
  getModulesByRol() {
    this.preloader = true;
    this.modulesByRolService.getAllModulesByService().subscribe(resp => {
      this.dataTable = resp.data;
      if (resp.status && resp.data != null) {
        this.dataSource = new MatTableDataSource(resp.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.changeFilterConditions();
      }
      this.preloader = false;
    }, error => {
      this.preloader = false;
      this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'error');
    });
  }



  
  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Cambiar las condiciones de busqueda
 **/
  changeFilterConditions() {
    this.dataSource.filterPredicate = (data, filter) => {
      if (this.changeModuleNames(data['fkModule'].uuid).toLowerCase().indexOf(filter) >= 0) {
        return true;
      }
      if (data['fkRol'].descriptionRol.toLowerCase().indexOf(filter) >= 0) {
        return true;
      }
      if (data['fkPermission'].permissionDescription.toLowerCase().indexOf(filter) >= 0) {
        return true;
      }

      return false;
    };
    console.log(typeof this.dataSource);
  }

  filterTable(event: Event) {
    const filter = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filter.trim().toLowerCase();
  }

  cleanSearchEngine() {
    if (this.validateFilters.get('search').value != '') {
      this.validateFilters.get('search').setValue('');
    }
  }


  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para cambiar la descripcion del modulo
 **/
  changeModuleNames(value) {
    switch (value) {
      case NameModules.holydayByCountry:
        return 'Festivos por país';
      case NameModules.countries:
        return 'Paises';
      case NameModules.customers:
        return 'Clientes';
      case NameModules.holydays:
        return 'Festivos';
      case NameModules.rol:
        return 'Roles';
      case NameModules.service:
        return 'Servicios';
      case NameModules.shelude:
        return 'Configuración horario';
      case NameModules.specialShedule:
        return 'Horario especial';
      case NameModules.user:
        return 'Usuarios';
      case NameModules.moduleByRol:
        return 'Modulos por Rol';
      default:
        return value;
    }
  }
}
