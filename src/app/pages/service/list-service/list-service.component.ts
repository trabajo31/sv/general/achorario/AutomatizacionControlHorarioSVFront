import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { BreakpointObserver } from '@angular/cdk/layout';
import { ServiceService } from '../../../services/service/service.service';
import { CreateServiceComponent } from '../modals/create-service/create-service.component';
import { NotificationsService } from '../../../shared/notifications/notificationsServices';
import { GlobalMethods } from '../../../global/globalmethods';
import { UntypedFormGroup, UntypedFormBuilder } from '@angular/forms';
import { ManagePermissions } from '../../../shared/validators/managePermissions';
import { NameModules } from '../../../model/permits.model';
import { MESSAGE_ALERT } from '../../../global/globalConfig';
import { ServicesModel } from '../../../model/services.model';

@Component({
  selector: 'app-list-service',
  templateUrl: './list-service.component.html',
  styleUrls: ['./list-service.component.css']
})
export class ListServiceComponent implements OnInit {
  preloader: boolean;
  displayedColumns = ['client', 'status', 'action'];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  public validateFilters: UntypedFormGroup;
  private allServices: ServicesModel;
  dataSource = new MatTableDataSource<any>();
  dataPermission: any;
  nameModules = NameModules;
  //Mensajes de alerta
  public message = MESSAGE_ALERT;


  constructor(breakpointObserver: BreakpointObserver,
    private serviceService: ServiceService,
    private dialog: MatDialog,
    private notificationsService: NotificationsService,
    private globalMethods: GlobalMethods,
    private formBuilder: UntypedFormBuilder,
    private managePermissions: ManagePermissions) {
    const columns = ['client', 'status', 'action'];
    breakpointObserver.observe(['(max-width: 600px)']).subscribe(() => {
      this.displayedColumns = columns;
    });
  }

  ngOnInit() {
    this.initForm();
    this.getData();
    this.managePermissions.getPermitsIndividual(NameModules.service);
  }

  /**
* @date(24-02-2020)
* @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
* @description Metodo encargado de controlar el filtro de busqueda.
**/
  initForm() {
    this.validateFilters = this.formBuilder.group({
      search: ['']
    });
  }

  /**
  * @date(22-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de obtener los datos de los servicios.
  **/
  getData() {
    this.preloader =true;
    this.serviceService.getAllService().subscribe(resp => {
      if (resp.status && resp.data != null) {
        this.dataSource = new MatTableDataSource(resp.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.allServices = resp.data;
      } else {
        this.notificationsService.alert('Error', resp.message, 'error');
      }
      this.preloader=false;
    }, () => {
      this.preloader =false;
      this.notificationsService.alert('Error', 'Error', 'error');
    });
  }

  /**
    * @date(12-07-2020)
    * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
    * @description Metodo encargado de abrir la modal para crear y actualizar filtro.
    * @param dataModal  objeto que contendra la data del registro seleccionado
  **/
  openModal(dataModal = null) {
    this.dataPermission = this.managePermissions.getModulePermissions();
    if (this.dataPermission.permission === 1) {
      this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'warning');
    } else if (this.dataPermission.permission === 2) {
      const dialogRef = this.dialog.open(CreateServiceComponent, {
        data: { dataModal, allServices: this.allServices },
        // data: { dataModal, allServices: this.allServices },
        width: '600px',
        // autoFocus: false,
        // disableClose: true
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result === 'acept') {
          this.ngOnInit();
        }
      });
    } else {
      this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'warning');
    }
  }

  /**
   * @date(12-07-2020)
   * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
   * @description Metodo encargado de refrescar los datos de la tabla
   **/
  refreshData() {
    this.cleanSearchEngine();
    this.getData();
  }

  /**
  * @date(12-07-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de abrir la modal para crear y actualizar filtro.
  * @param event  objeto que contendra el evento para activar la busqueda.
  **/
  filterTable(event: Event) {
    const filter = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filter.trim().toLowerCase();
  }

  /**
   * @date(12-07-2020)
   * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
   * @description Metodo encargado de limpiar el campo de busqueda
   **/
  cleanSearchEngine() {
    if (this.validateFilters.get('search').value != '') {
      this.validateFilters.get('search').setValue('');
    }
  }
}
