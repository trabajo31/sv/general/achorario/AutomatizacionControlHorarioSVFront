import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { v4 as uuidv4 } from 'uuid';
import { ServiceService } from '../../../../services/service/service.service';
import { NotificationsService } from '../../../../shared/notifications/notificationsServices';
import { CreateRolComponent } from '../../../rol/modals/create-rol/create-rol.component';
import { GlobalMethods } from '../../../../global/globalmethods';
import { MESSAGE_ALERT } from '../../../../global/globalConfig';
import { ServicesModel } from '../../../../model/services.model';
import { 
  virtualServiceValidator, 
  validateXSS, 
  validateNameService 
} from '../../../../services/utils/directives/form-validations.directive';


@Component({
  selector: 'app-create-service',
  templateUrl: './create-service.component.html',
  styleUrls: ['./create-service.component.css']
})
export class CreateServiceComponent implements OnInit {

  preloader: boolean;
  //Objeto para controlar filtros.
  public validateFilters: UntypedFormGroup;
  //Objetos que controlan la modal.
  public statusActivated: boolean = false;
  //Titulo de formulario
  public titleModal: string;
  //Filtros de opciones
  public filterOptions = this.globalMethods.filterOptions;
  //Validador si la modal es para actualziar
  public modalUpdate: boolean = false;
  //Mensajes de alerta
  public message = MESSAGE_ALERT;
  //Objeto para el control de datos de envio
  private objectDataUpdate = {
    id: "",
    name: "",
    state: 0,
    uid: ""
  };

  public clienValidate = 'servicio';

  private allServices: ServicesModel[];
  private servicesToUpdate: ServicesModel;
  
  constructor(
    private serviceService: ServiceService,
    private fb: UntypedFormBuilder,
    private notificationsService: NotificationsService,
    public dialogRef: MatDialogRef<CreateRolComponent>,
    private globalMethods: GlobalMethods,
    @Inject(MAT_DIALOG_DATA) public dataUpdate
  ) { }

  ngOnInit() {
    this.allServices = this.dataUpdate.allServices;
    this.servicesToUpdate = this.dataUpdate.dataModal;
    this.initForm();
    this.validateTypeModal();
  }

  /**
  * @date(24-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de inicializar los filtros.
  **/
  initForm() {
    this.validateFilters = this.fb.group({
      service: ['', [Validators.required, virtualServiceValidator(this.clienValidate), validateXSS(), validateNameService(this.allServices, this.servicesToUpdate)]],
      state: ['', Validators.required]
    });
  }

  /**
  * @date(22-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de de validar si la modal que abrira sera de  crear o actualziar,
  * al igual que precargara los datos en caso de que sea  actualizar.
  **/
  validateTypeModal() {
    if (this.servicesToUpdate != undefined && this.servicesToUpdate != null) {
      this.titleModal = "Actualizar servicio";
      this.statusActivated = true;
      this.modalUpdate = true;
      this.preloadDataUpdate();
    } else {
      this.titleModal = "Crear nuevo servicio";
    }
  }

  /**
  * @date(22-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de precargar los datos en la modal de Actualizar.
  **/
  preloadDataUpdate() {
    this.validateFilters.get('service').setValue(this.servicesToUpdate.name);
    this.validateFilters.get('state').setValue(this.servicesToUpdate.state);
  }

  /**
  * @date(22-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de guardar los datos.
  **/
  option() {
    if (this.checkFilters(this.modalUpdate)) {
      const params = this.objectDataUpdate;
      if (this.modalUpdate) {
        params.id = this.servicesToUpdate.id;
        params.name = this.validateFilters.get('service').value.toUpperCase();
        params.state = this.validateFilters.get('state').value;
        params.uid = this.servicesToUpdate.uid;
        this.update(params);
      } else {
        var idData = uuidv4();
        params.id = idData;
        params.name = this.validateFilters.get('service').value.toUpperCase();
        params.state = 1;
        params.uid = idData;
        this.save(params);
      }
    } else {
      this.notificationsService.alert('Aviso', this.message.mandatory, 'warning');
    }
  }

  /**
  * @date(22-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de actualziar los datos
  **/
  update(dataToUpdate: ServicesModel) {
    this.serviceService.update(dataToUpdate).subscribe(resp => {
      this.closemodal();
      if(resp) {
        this.notificationsService.alert('Aviso', this.message.update, 'success');
      }
    }, error => {
      if (error.status === 500) {
        this.notificationsService.alert('Aviso', this.message.duplicate, 'warning');
      } else {
        this.notificationsService.alert('Aviso', this.message.error, 'error');
      }
    });
  }

  /**
  * @date(22-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de guardar los datos.
  **/
  save(dataToSave: ServicesModel) {
    this.preloader = true;
    this.serviceService.save(dataToSave).subscribe(resp => {
      this.closemodal();
      if(resp) {
        this.notificationsService.alert('Aviso', this.message.add, 'success');
      }
    }, error => {
      if (error.status === 500) {
        this.notificationsService.alert('Aviso', this.message.duplicate, 'warning');
        this.preloader =false;
      } else {
        this.notificationsService.alert('Aviso', this.message.error, 'error');
        this.preloader = false;
      }
    });
  }

  /**
  * @date(24-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de verificar si todos los filtros fueron validos.
  **/
  checkFilters(option: boolean): boolean {
    if (option) {
      if (this.validateFilters.valid) {
        return true;
      } else {
        return false;
      }
    } else {
      if (this.validateFilters.get('service').valid) {
        return true;
      } else {
        return false;
      }
    }
  }

  /**
  * @date(22-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de cerrar la modal.
  **/
  closemodal() {
    this.dialogRef.close('acept');
  }

  /**
  * @date(29-09-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de validar si entran caracteres diferentes a numeros
  **/
  validateNumbers(event) {
    return this.globalMethods.validateOnlyNumbers(event);
  }

  /**
  * @date(29-09-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de validar la longitud de la cadena de texto.
  **/
  validateSize(field: string, value: string, maxSize: number) {
    return this.globalMethods.validateCharacterSize(field, value, maxSize);
  }

  get formValidator() {   
    return this.validateFilters.controls; 
  }


}
