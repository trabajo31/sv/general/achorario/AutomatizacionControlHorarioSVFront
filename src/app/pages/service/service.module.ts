import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { ServiceRoutingModule } from './service-routing.module';
import { ListServiceComponent } from './list-service/list-service.component';
import { CreateServiceComponent } from './modals/create-service/create-service.component';

@NgModule({
    declarations: [ListServiceComponent, CreateServiceComponent],
    imports: [
        CommonModule,
        SharedModule,
        ServiceRoutingModule
    ]
})
export class ServiceModule { }
