import { Component, OnInit, Inject } from '@angular/core';
import { UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { GlobalMethods } from 'src/app/global/globalmethods';
import { NotificationsService } from 'src/app/shared/notifications/notificationsServices';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CustomerService } from 'src/app/services/customers/customer.service';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.css']
})
export class CreateCustomerComponent implements OnInit {

  public validateFilters: UntypedFormGroup;

  public statusActivated: Boolean;
  public titleModal: String;
  public filterOptions;
  public modalUpdate: Boolean;

  private objectDataUpdate = {
    id: 0,
    name: '',
    state: 0,
    uid: 0,
  };

  constructor(
    private globalMethods: GlobalMethods,
    private formBuilder: UntypedFormBuilder,
    private notificationsService: NotificationsService,
    public dialogRef: MatDialogRef<CreateCustomerComponent>,
    private customersService: CustomerService,
    @Inject(MAT_DIALOG_DATA) public dataUpdate: any
  ) {
    this.statusActivated = false;
    this.filterOptions = this.globalMethods.filterOptions;
  }

  ngOnInit() {
    this.initForm();
    this.validateTypeModal();
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para inicializar el formulario
 **/
  initForm() {
    this.validateFilters = this.formBuilder.group({
      customer: ['', [Validators.required, Validators.pattern('[a-zA-ZÑáéíóú ]*')]],
      state: ['', Validators.required]
    });

  }

  inputUpper(formControlName, event) {
    this.validateFilters.get(formControlName).setValue(event.target.value.toUpperCase());

  }


  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para validar el tipo de modal (update o nuevo registro)
 **/
  validateTypeModal() {
    if (this.dataUpdate !== undefined && this.dataUpdate !== null && this.dataUpdate !== '') {
      this.titleModal = 'Actualizar cliente';
      this.statusActivated = true;
      this.modalUpdate = true;
      this.preloadDataUpdate();
    } else {
      this.titleModal = 'Agregar nuevo cliente';
    }
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para guardar un nuevo registro o actualizar uno anterior
 **/
  save() {
    if (this.checkFilters(this.modalUpdate)) {
      const params = this.objectDataUpdate;
      if (this.modalUpdate) {
        params.id = this.dataUpdate.id;
        params.name = this.validateFilters.get('customer').value.toUpperCase();
        params.state = this.validateFilters.get('state').value;
        params.uid = this.dataUpdate.uid;
        this.updateCustomer(params);
      } else {
        const idData = uuidv4();
        params.id = idData;
        params.name = this.validateFilters.get('customer').value.toUpperCase();
        params.state = 1;
        params.uid = idData;
        this.saveCustomer(params);
      }
    } else {
      this.notificationsService.alert('Aviso', 'Los campos marcados son obligatorios', 'warning');
    }
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para guardar un nuevo registro
 **/
  saveCustomer(params) {
    this.customersService.save(params).subscribe(resp => {
      if (resp.responseCode === 400) {
        this.notificationsService.alert('Aviso', 'Registro fue previamente realizado', 'warning');
        return;
      }
      this.closemodal();
      this.notificationsService.alert('Aviso', 'Datos almacenados correctamente', 'success');
    }, error => {
      this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'error');
    });
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para actualizar un registro
 **/
  updateCustomer(params) {
    this.customersService.update(params).subscribe(resp => {
      this.closemodal();
      this.notificationsService.alert('Aviso', 'Datos actualizados correctamente', 'success');
    }, error => {
      this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'error');
    });
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para pre cargar el modal en caso de que sea un update
 **/
  preloadDataUpdate() {
    this.validateFilters.get('customer').setValue(this.dataUpdate.name);
    this.validateFilters.get('state').setValue(this.dataUpdate.state);
  }

  checkFilters(option: Boolean): Boolean {
    if (option) {
      if (this.validateFilters.valid) {
        return true;
      } else {
        return false;
      }
    } else {
      if (this.validateFilters.get('customer').valid) {
        return true;
      } else {
        return false;
      }
    }
  }

  closemodal() {
    this.dialogRef.close('acept');
  }



}
