import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomersRoutingModule } from './customers-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { CreateCustomerComponent } from './modals/create-customer/create-customer.component';
// Angular node v14.15.5
// npm 6.14.11
@NgModule({
    imports: [
        CommonModule,
        CustomersRoutingModule,
        SharedModule
    ],
    declarations: [
        CustomerListComponent,
        CreateCustomerComponent
    ]
})
export class CustomersModule { }
