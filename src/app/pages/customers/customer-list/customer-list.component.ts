import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { BreakpointObserver } from '@angular/cdk/layout';
import { CustomerService } from 'src/app/services/customers/customer.service';
import { GlobalMethods } from 'src/app/global/globalmethods';
import { NotificationsService } from 'src/app/shared/notifications/notificationsServices';
import { CreateCustomerComponent } from '../modals/create-customer/create-customer.component';
import { ManagePermissions } from 'src/app/shared/validators/managePermissions';
import { NameModules } from '../../../model/permits.model';
import { MESSAGE_ALERT } from '../../../global/globalConfig';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  preloader:boolean;
  public validateFilters: UntypedFormGroup;
  public permitions: number;
  dataPermission: any;
  nameModules = NameModules;
  //Mensajes de alerta
  public message = MESSAGE_ALERT;

  displayedColumns = ['customer', 'status', 'action'];

  dataSource: MatTableDataSource<any>;

  private objectDataDelete = {
    id: 0,
    name: '',
    state: 0,
    uid: 0,
  };

  constructor(
    private breakPointObserver: BreakpointObserver,
    private customersService: CustomerService,
    private formBuilder: UntypedFormBuilder,
    private globalMethods: GlobalMethods,
    private notificationsService: NotificationsService,
    private dialog: MatDialog,
    private managePermissions: ManagePermissions
  ) {
    const columns = ['customer', 'status', 'action'];
    this.breakPointObserver.observe(['(max-width: 600px)'])
      .subscribe(result => this.displayedColumns = columns);
  }

  ngOnInit() {
    this.initForm();
    this.getData();
    this.managePermissions.getPermitsIndividual(NameModules.customers);
  }

  initForm() {
    this.validateFilters = this.formBuilder.group({
      search: ['', [Validators.required, Validators.pattern('[a-zA-ZÑáéíóú ]*')]]
    });
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para obtener información para la tabla
 **/
  getData() {
    this.preloader = true;
    this.customersService.getAllCustomers().subscribe(resp => {

      if (resp.data !== undefined && resp.data !== null) {
        this.dataSource = new MatTableDataSource(resp.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
      this.preloader = false;
    });
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para eliminar(inactivar) un registro
 **/
  deleteCustomer(data: any = null) {
    if (data !== null) {
      this.customersService.delete(data).subscribe(resp => {
        this.notificationsService.alert('Aviso', 'Registro eliminado correctamente', 'success');
        this.cleanSearchEngine();
        this.getData();
      }, error => {
        this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'error');
      });
    }
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para abrir un nuevo modal de actualizar o agregar un nuevo registro
 **/
  openModal(dataModal = null) {
    this.dataPermission = this.managePermissions.getModulePermissions();
    if (this.dataPermission.permission === 1) {
      this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'warning');
    } else if (this.dataPermission.permission === 2) {
      let dialogRef;
      dialogRef = this.dialog.open(CreateCustomerComponent, {
        data: dataModal,
        height: '200px',
        width: '600px',
        autoFocus: false,
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result === 'acept') {
          this.ngOnInit();
        }
      });
    } else {
      this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'warning');
    }
  }



  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para cambiar la forma de filtrar la tabla
 **/
  filterTable(event: Event) {
    const filter = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filter.trim().toLowerCase();
  }

  cleanSearchEngine() {
    if (this.validateFilters.get('search').value !== '') {
      this.validateFilters.get('search').setValue('');
    }
  }


}
