import { Component, OnInit, Inject } from '@angular/core';
import { RolService } from 'src/app/services/rol/rol.service';
import { UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { NotificationsService } from 'src/app/shared/notifications/notificationsServices';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GlobalMethods } from 'src/app/global/globalmethods';
import { v4 as uuidv4 } from 'uuid';
import { MESSAGE_ALERT } from '../../../../global/globalConfig';

@Component({
  selector: 'app-create-rol',
  templateUrl: './create-rol.component.html',
  styleUrls: ['./create-rol.component.css']
})
export class CreateRolComponent implements OnInit {


  preloader: boolean = false;
  //Objeto para controlar filtros.
  public validateFilters: UntypedFormGroup;
  //Objetos que controlan la modal.
  public statusActivated: boolean = false;
  //Titulo modal
  public titleModal: string;
  //Opciones de filtros
  public filterOptions = this.globalMethods.filterOptions;
  //Validar si la modal sera para actualizar
  public modalUpdate: boolean = false;
  //Mensajes de alerta
  public message = MESSAGE_ALERT;
  //Objeto para el control de datos de envio
  public objectDataUpdate = {
    id: 0,
    name: "",
    state: 0,
    uid: 0,
  }

  constructor(
    private rolService: RolService,
    private fb: UntypedFormBuilder,
    private notificationsService: NotificationsService,
    public dialogRef: MatDialogRef<CreateRolComponent>,
    private globalMethods: GlobalMethods,
    @Inject(MAT_DIALOG_DATA) public dataUpdate: any,
  ) { }

  ngOnInit() {
    this.initForm();
    this.validateTypeModal();
  }

  /**
  * @date(24-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de inicializar los filtros.
  **/
  initForm() {
    this.validateFilters = this.fb.group({
      rol: ['', [Validators.required, Validators.pattern('[a-zA-ZÑáéíóú ]*')]],
      state: ['', Validators.required]
    });
  }

  /**
  * @date(22-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de de validar si la modal que abrira sera de  crear o actualziar,
  * al igual que precargara los datos en caso de que sea  actualizar.
  **/
  validateTypeModal() {
    if (this.dataUpdate != undefined && this.dataUpdate != null && this.dataUpdate != '') {
      this.titleModal = "Actualizar Rol";
      this.statusActivated = true;
      this.modalUpdate = true;
      this.preloadDataUpdate();
    } else {
      this.titleModal = "Crear Nuevo Rol";
    }
  }

  /**
  * @date(22-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de precargar los datos en la modal de Actualizar.
  **/
  preloadDataUpdate() {
    this.validateFilters.get('rol').setValue(this.dataUpdate.name);
    this.validateFilters.get('state').setValue(this.dataUpdate.state);
  }

  /**
  * @date(22-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de validar que opcion realizar(Update/Save)
  **/
  option() {
    
    if (this.checkFilters(this.modalUpdate)) {
      let params = this.objectDataUpdate;
      if (this.modalUpdate) {
        params.id = this.dataUpdate.id;
        params.name = this.validateFilters.get('rol').value.toUpperCase();
        params.state = this.validateFilters.get('state').value;
        params.uid = this.dataUpdate.uid;
        this.update(params);
      } else {
        var idData = uuidv4();
        params.id = idData;
        params.name = this.validateFilters.get('rol').value.toUpperCase();
        params.state = 1;
        params.uid = idData;
        
        this.save(params);
      }
    } else {
      this.preloader = false;
      this.notificationsService.alert('Aviso', this.message.mandatory, 'warning');
    }
  }

  /**
    * @date(22-02-2020)
    * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
    * @description Metodo encargado de actualizar los datos
    **/
  update(dataToSave: any) {
    this.rolService.update(dataToSave).subscribe(resp => {
      this.closemodal();
      this.notificationsService.alert('Aviso', this.message.update, 'success');
    }, error => {
      if (error.status === 500) {
        this.notificationsService.alert('Aviso', this.message.duplicate, 'warning');
      } else {
        this.notificationsService.alert('Aviso', this.message.error, 'error');
      }
    });
  }

  /**
  * @date(22-02-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de guardar los datos.
  **/
  save(dataToSave: any) {
    this.preloader = true;
    this.rolService.save(dataToSave).subscribe(resp => {
      this.closemodal();
      this.notificationsService.alert('Aviso', this.message.add, 'success');
    }, error => {
      this.preloader = false;
      if (error.status === 500) {
        this.notificationsService.alert('Aviso', this.message.duplicate, 'warning');
      } else {
        this.preloader = false;
        this.notificationsService.alert('Aviso', this.message.error, 'error');
      }
    });
  }

  /**
   * @date(24-02-2020)
   * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
   * @description Metodo encargado de verificar si todos los filtros fueron validos.
   **/
  checkFilters(option: boolean): boolean {
    if (option) {
      if (this.validateFilters.valid) {
        return true;
      } else {
        return false;
      }
    } else {
      if (this.validateFilters.get('rol').valid) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  /**
    * @date(22-02-2020)
    * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
    * @description Metodo encargado de cerrar la modal.
    **/
  closemodal() {
    this.dialogRef.close('acept');
  }

  /**
  * @date(29-09-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de validar si entran caracteres diferentes a numeros
  **/
  validateNumbers(event) {
    return this.globalMethods.validateOnlyNumbers(event);
  }

  /**
  * @date(29-09-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de validar la longitud de la cadena de texto.
  **/
  validateSize(field: string, value: any, maxSize: number) {
    return this.globalMethods.validateCharacterSize(field, value, maxSize);
  }

}
