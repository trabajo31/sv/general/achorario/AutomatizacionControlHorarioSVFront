import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { RolRoutingModule } from './rol-routing.module';
import { ListRolComponent } from './list-rol/list-rol.component';
import { CreateRolComponent } from './modals/create-rol/create-rol.component';



@NgModule({
    declarations: [ListRolComponent, CreateRolComponent],
    imports: [
        CommonModule,
        SharedModule,
        RolRoutingModule,
    ]
})
export class RolModule { }
