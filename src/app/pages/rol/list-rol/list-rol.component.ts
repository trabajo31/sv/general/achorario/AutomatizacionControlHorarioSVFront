import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';

import { BreakpointObserver } from '@angular/cdk/layout';
import { RolService } from 'src/app/services/rol/rol.service';
import { CreateRolComponent } from '../modals/create-rol/create-rol.component';
import { NotificationsService } from 'src/app/shared/notifications/notificationsServices';
import { GlobalMethods } from '../../../global/globalmethods';
import { UntypedFormGroup, UntypedFormBuilder } from '@angular/forms';
import { ManagePermissions } from 'src/app/shared/validators/managePermissions';
import { NameModules } from '../../../model/permits.model';
import { MESSAGE_ALERT } from '../../../global/globalConfig';

@Component({
  selector: 'app-list-rol',
  templateUrl: './list-rol.component.html',
  styleUrls: ['./list-rol.component.css']
})
export class ListRolComponent implements OnInit {
  
  preloader:boolean;
  // Objetos encargados de paginar y organizar la tabla.
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  // Objeto para agrupar filtros
  public validateFilters: UntypedFormGroup;
  //Objeto con columnas de la tabla.
  displayedColumns = ['rol', 'status', 'action'];
  // Data de la tabla de registros
  dataSource = new MatTableDataSource<any>();
  // Objeto para controlar la data de envio.
  public loading:boolean = false;
  private objectDataDelete = {
    id: 0,
    name: "",
    state: 0,
    uid: 0,
  }
  dataPermission: any;
  nameModules = NameModules;

  //Mensajes de alerta
  public message = MESSAGE_ALERT;

  constructor(breakpointObserver: BreakpointObserver,
    private RolService: RolService,
    private dialog: MatDialog,
    private notificationsService: NotificationsService,
    private globalMethods: GlobalMethods,
    private formBuilder: UntypedFormBuilder,
    private managePermissions: ManagePermissions) {
    let columns = ['rol', 'status', 'action'];
    breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
      this.displayedColumns = columns;
    });
  }

  ngOnInit() {
    this.initForm();
    this.getData();
    this.managePermissions.getPermitsIndividual(NameModules.rol);
  }

  /**
 * @date(12-07-2020)
 * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
 * @description Metodo encargado de inicializar el campo de busqueda
 **/
  initForm() {
    this.validateFilters = this.formBuilder.group({
      search: ['']
    });
  }

  /**
  * @date(12-07-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de obtener la data  de Roles.
  **/
  getData() {
    this.preloader = true;
    this.RolService.getAllService().subscribe(resp => {
      if (resp.status && resp.data != null) {
        this.dataSource = new MatTableDataSource(resp.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
      this.preloader = false;
    }, error => {
      this.preloader = false;
      this.notificationsService.alert('Aviso', this.message.error, 'error');
    });
  }

  /**
  * @date(12-07-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de refrescar los datos de la tabla
  **/
  refreshData() {
    this.cleanSearchEngine();
    this.getData();
  }


  /**
  * @date(12-07-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de abrir la modal para crear y actualizar filtro.
  * @param dataModal  objeto que contendra la data del registro seleccionado
  **/
  openModal(dataModal = null) {
    this.dataPermission = this.managePermissions.getModulePermissions();
    if (this.dataPermission.permission === 1) {
      this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'warning');
    } else if (this.dataPermission.permission === 2) {
      let dialogRef;
      dialogRef = this.dialog.open(CreateRolComponent, {
        data: dataModal,
        height: '200px',
        width: '600px',
        autoFocus: false,
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result === 'acept') {
          this.ngOnInit();
        }
      });
    } else {
      this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'warning');
    }
  }

  /**
  * @date(12-07-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de abrir la modal para crear y actualizar filtro.
  * @param event  objeto que contendra el evento para activar la busqueda.
  **/
  filterTable(event: Event) {
    const filter = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filter.trim().toLowerCase();
  }

  /**
  * @date(12-07-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de limpiar el campo de busqueda
  **/
  cleanSearchEngine() {
    if (this.validateFilters.get('search').value != '') {
      this.validateFilters.get('search').setValue('');
    }
  }

}
