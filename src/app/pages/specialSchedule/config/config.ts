export let ModelSpecialShedule = {

    /**pk_special_schedule:,
    id_Special_Schedule:,
    **/
    pk_special_schedule: 0,
    id_Special_Schedule: 0,
    fk_customer: {},
    fk_service: {},
    fk_holyday: {},
    stard_hour_special: new Date(),
    end_hour_special: new Date(),
    special_message: "",
    status_special_schedule: 0

}

export interface specialShedule {

    pk_special_schedule:String,
    id_Special_Schedule: String,
    fk_customer: String,
    fk_service: String,
    fk_holyday: String,
    stard_hour_special:String
    end_hour_special: String
    special_message: String
    status_special_schedule:0
}

