import { Component, OnInit, ViewChild } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { NotificationsService } from 'src/app/shared/notifications/notificationsServices';
import { GlobalMethods } from '../../../global/globalmethods';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UntypedFormGroup, UntypedFormBuilder } from '@angular/forms';
import { CreateSpecialScheduleComponent } from '../modals/create-specialSchedule/create-specialSchedule.component';
import { SpecialScheduleService } from 'src/app/services/specialSchedule/specialSchedule.service';
import { AlertService } from 'src/app/services/Useful/alert-service';
import { ManagePermissions } from 'src/app/shared/validators/managePermissions';
import { NameModules } from '../../../model/permits.model';
import { specialShedule } from '../config/config';
import { SelectionModel } from '@angular/cdk/collections';
import { MESSAGE_ALERT } from '../../../global/globalConfig';


@Component({
  selector: 'app-list-SpecialSchedule',
  templateUrl: './list-specialSchedule.component.html',
  styleUrls: ['./list-specialSchedule.component.css']
})
export class ListSpecialScheduleComponent implements OnInit {

  preloader: boolean = false;


  // Objetos encargados de paginar y organizar la tabla.
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  // Objeto para agrupar filtros
  public validateFilters: UntypedFormGroup;
  //Objeto con columnas de la tabla.
  displayedColumns = ['select', 'client', 'service', 'date', 'start_time', 'end_time', 'status', 'actions'];
  // Data de la tabla de registros
  selection = new SelectionModel<specialShedule>(true, []);
  dataSource = new MatTableDataSource([]);
  idspecialSelected = [];
  // Objeto para controlar la data de envio.
  dataPermission: any;
  nameModules = NameModules;
  checkboxOptions = [{ value: 'disable', name: 'desactivar estados', icon: 'not_interested', color: 'warn' }];


  //Mensajes de alerta
  public message = MESSAGE_ALERT;


  constructor(breakpointObserver: BreakpointObserver,
    private specialScheduleService: SpecialScheduleService,
    private dialog: MatDialog,
    private alertService: AlertService,
    private managePermissions: ManagePermissions,
    private notificationsService: NotificationsService,
    private globalMethods: GlobalMethods,
    private formBuilder: UntypedFormBuilder
  ) {
    let columns = ['select', 'client', 'service', 'date', 'start_time', 'end_time', 'status', 'actions'];
    breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
      this.displayedColumns = columns;
    });
  }

  ngOnInit() {
    this.initForm();
    this.getData();
    this.managePermissions.getPermitsIndividual(NameModules.specialShedule);
  }

  /**
 * @date(12-07-2020)
 * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
 * @description Metodo encargado de inicializar el campo de busqueda
 **/
  initForm() {
    this.validateFilters = this.formBuilder.group({
      search: ['']
    });
  }

  /**
   * @date(12-07-2020)
   * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
   * @description Metodo encargado de obtener la data  de horario espescial.
   **/
  getData() {
    this.preloader = true;

    this.specialScheduleService.getAllSpecialSchedule().subscribe(resp => {
      this.preloader = false;
      if (resp.data != undefined && resp.data != null) {
        this.dataSource = new MatTableDataSource(resp.data);

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        this.changeFilterConditions();
      }
      this.preloader = false;

    }, error => {
      this.preloader = false;
      this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'error');
    });

  }

  /**
     * @date(22-08-2020)
     * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
     * @description Metodo encargado  de cambiar el estado de activo a inactivo
     * @param idSpecialSchedule  Variable contenedora.
   **/
  disableStatusSpecialSchedule() {
    this.alertService.confirm(
      '¿Está seguro desactivar el estado horario especial',
      '',
      'warning'
    ).then(res => {
      if (res) {
        let selecionados = [];
        const numSelected = this.selection.selected;

        if (numSelected.length > 0) {
          numSelected.map(
            selected => selecionados.push({ id: selected.id_Special_Schedule })
          );
        }

        this.dataSource = new MatTableDataSource<specialShedule>([]);
        this.specialScheduleService.deleteSpecialShedule(selecionados)
          .subscribe(resp => {
            if (resp.status) {
              this.getData();
              this.notificationsService.alert('', resp.message, 'success');
            } else {
              this.notificationsService.alert('', resp.message, 'error');
            }
            this.getData();
          }, error => {
            this.getData();
          });
      }

    });
  }


  /**
* @date(10-07-2020)
* @author Jesus David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
* @description Metodo encargado de abrir la modal para crear y actualizar filtro.
* @param dataModal  objeto que contendra la data del registro seleccionado
**/
  openModal(dataModal = null) {
    this.dataPermission = this.managePermissions.getModulePermissions();
    if (this.dataPermission.permission === 1) {
      this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'warning');
    } else if (this.dataPermission.permission === 2) {
      const dialogRef = this.dialog.open(CreateSpecialScheduleComponent, {
        data: dataModal,
        height: '500px',
        width: '800px',
        autoFocus: false,
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result === 'acept') {
          this.ngOnInit();
        }
      });
    } else {
      this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'warning');
    }
  }

  /**
* @date(18-07-2020)
* @author Jesús David Sanchez Pabón <jesus.sanchezgrupokonecta.com>
* @description Metodo encargado de abrir la modal para crear y actualizar filtro.
* @param event  objeto que contendra el evento para activar la busqueda.
**/
  filterTable(event: Event) {
    const filter = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filter.trim().toLowerCase();
  }

  /**
* @date(12-07-2020)
* @author Jesús David Sanchez Pabón <jesus.sanchezgrupokonecta.com>
* @description Metodo encargado de refrescar los datos de la tabla
**/
  refreshData() {
    this.cleanSearchEngine();
    this.getData();
  }



  /**
   * @date(18-07-2020)
   * @author Jesús David Sanchez Pabón <jesus.sanchezgrupokonecta.com>
   * @description Metodo encargado buscar por cliente .
   */
  changeFilterConditions() {
    this.dataSource.filterPredicate = (data, filter) => {

      if (data.fk_customer.nameCustomer.toLowerCase().indexOf(filter) >= 0) {
        return true;
      }
      return false;
    };
  }


  /**
   * @date(18-07-2020)
   * @author Jesús David Sanchez Pabón <jesus.sanchezgrupokonecta.com>
   * @description Metodo encargado selecionar en checkbox.
   */
  isAllSelected() {

    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;

    return numSelected === numRows;
  }

  /**
   * @date(18-07-2020)
   * @author Jesús David Sanchez Pabón <jesus.sanchezgrupokonecta.com>
   * @description Selecciona todas las filas si no están todas seleccionadas; 
   * de lo contrario, selección clara.
   */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));

  }

  /**
   * @date(18-07-2020)
   * @author Jesús David Sanchez Pabón <jesus.sanchezgrupokonecta.com>
   * @description metodo encargado realizar las acciones del inactivo
   * @param action 
   */
  actionCasesSelected(action) {

    this.idspecialSelected = [];

    for (const i in this.selection.selected) {
      if (this.selection.selected.hasOwnProperty(i)) {
        this.idspecialSelected.push(this.selection.selected[i]['id_Special_Schedule']);
      }

    }
    switch (action) {

      case 'disable':
        this.disableStatusSpecialSchedule();
        break;
      default:
        break;
    }
  }


  /**
  * @date(18-07-2020)
  * @author Jesús David Sanchez Pabón <jesus.sanchezgrupokonecta.com>
  * @description Metodo encargado de limpiar el campo de busqueda
  **/
  cleanSearchEngine() {
    if (this.validateFilters.get('search').value != '') {
      this.validateFilters.get('search').setValue('');
    }
  }

}
