import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import {MatTableModule} from '@angular/material/table';
import { SpecialScheduleRouting } from './specialSchedule.routing';
import { ListSpecialScheduleComponent } from './list-specialSchedule/list-specialSchedule.component';
import { CreateSpecialScheduleComponent } from './modals/create-specialSchedule/create-specialSchedule.component';





@NgModule({
    declarations: [ListSpecialScheduleComponent, CreateSpecialScheduleComponent],
    imports: [
        CommonModule,
        SpecialScheduleRouting,
        SharedModule,
        MatTableModule,
    ],
    providers: [
        DatePipe
    ]
})
export class SpecialScheduleModule { }
