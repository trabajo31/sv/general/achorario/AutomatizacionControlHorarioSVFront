import { Component, OnInit, Inject } from '@angular/core';
import { UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { NotificationsService } from 'src/app/shared/notifications/notificationsServices';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GlobalMethods } from 'src/app/global/globalmethods';
import { ServiceService } from 'src/app/services/service/service.service';
import { ModelSpecialShedule } from '../../config/config';
import { SpecialScheduleService } from 'src/app/services/specialSchedule/specialSchedule.service';
import { CustomerService } from 'src/app/services/customers/customer.service';
import { HolydayService } from 'src/app/services/holyday/holyday.service';
import { DatePipe } from '@angular/common';
import { v4 as uuidv4 } from 'uuid';
import * as moment from 'moment';
import { MESSAGE_ALERT } from 'src/app/global/globalConfig';


@Component({
  selector: 'app-create-specialSchedule',
  templateUrl: './create-specialSchedule.component.html',
  styleUrls: ['./create-specialSchedule.component.css']
})
export class CreateSpecialScheduleComponent implements OnInit {
  //Objeto para controlar filtros.
  validateFilters: UntypedFormGroup;
  //Objetos que controlan la modal.
  statusActivated: boolean = false;
  //Mensajes de alerta
  public message = MESSAGE_ALERT;
  //Titulo modal
  titleModal: string;
  private filterOptions = this.globalMethods.filterOptions;
  private modalUpdate: boolean = false;

  preloader: boolean = false;


  //Objeto para el control de datos de envio
  dataCustomer: [];
  dataService: [];
  dataHolyday: [];
  myDate
  minDate = moment(new Date()).format('YYYY-MM-DD');


  private customerValue: any;
  //Objeto para el control de datos de envio
  private serviceValue: any;

  private holydayValue: any;

  private objectDataUpdate = {
    pk_special_schedule: 0,
    id_Special_Schedule: 0,
    fk_customer: {},
    fk_service: {},
    fk_holyday: {},
    stard_hour_special: new Date(),
    end_hour_special: new Date(),
    special_message: "",
    status_special_schedule: 0

  }

  constructor(
    private specialScheduleService: SpecialScheduleService,
    private serviceService: ServiceService,
    private customerService: CustomerService,
    private holydayService: HolydayService,
    private fb: UntypedFormBuilder,
    private notificationsService: NotificationsService,
    public dialogRef: MatDialogRef<CreateSpecialScheduleComponent>,
    private globalMethods: GlobalMethods,
    private datePipe: DatePipe,
    @Inject(MAT_DIALOG_DATA) public dataUpdate: any,
  ) {

    this.getDateCustomer();
    this.getDateService();
    this.getDateHolyday();

  }
  ngOnInit() {
    this.initForm();
    this.validateTypeModal();

  }

  validarfeche(seleciona: any) {


  }
  selectClient(c1, c2) {

    if (c1.id === c2.idCustomer) {
      this.customerValue = c1;
      return true;
    }
    return false;
  }

  selectService(c1, c2) {

    if (c1.id === c2.idService) {
      this.serviceValue = c1;
      return true;
    }
    return false;
  }
  selectDate(c1, c2) {
    if (c1.idh === c2.idHolyday) {
      this.holydayValue = c1;
      return true;
    }
    return false;
  }

  /**
  * @date(18-02-2020)
  * @author  Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
  * @description Metodo encargado de inicializar los filtros.
  **/
  initForm() {
    this.validateFilters = this.fb.group({
      client: ['', Validators.required],
      service: ['', Validators.required],
      date: ['', Validators.required],
      start_time: ['', Validators.required],
      end_time: ['', Validators.required],
      message: ['', [Validators.required, Validators.pattern('^((?!javascript).)*$'), Validators.pattern('^((?!JAVASCRIPT).)*$')]],
      state: ['', Validators.required]

    });

  }

  /**
  * @date(18-02-2020)
  * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
  * @description Metodo encargado de precargar los datos en la modal de Actualizar.
  **/
  preloadDataUpdate() {

    this.validateFilters.get('client').setValue(this.dataUpdate.fk_customer);
    this.validateFilters.get('client').disable();
    this.validateFilters.get('service').setValue(this.dataUpdate.fk_service);
    this.validateFilters.get('service').disable();
    this.validateFilters.get('date').setValue(this.dataUpdate.fk_holyday);
    this.validateFilters.get('date').disable();

    let time = this.datePipe.transform(new Date(this.dataUpdate.stard_hour_special), 'hh:mm');
    this.validateFilters.get('start_time').setValue(time);

    time = this.datePipe.transform(new Date(this.dataUpdate.end_hour_special), 'hh:mm');
    this.validateFilters.get('end_time').setValue(time);
    this.validateFilters.get('message').setValue(this.dataUpdate.special_message);
    this.validateFilters.get('state').setValue(this.dataUpdate.status_special_schedule);

    this.customerValue = {
      "id": this.dataUpdate.fk_customer.idCustomer,
      "name": this.dataUpdate.fk_customer.nameCustomer,
      "state": this.dataUpdate.fk_customer.stateCustomer,
      "uid": this.dataUpdate.fk_customer.pkCustomer

    }
    this.serviceValue = {
      "uid": this.dataUpdate.fk_service.pkService,
      "name": this.dataUpdate.fk_service.descriptionService,
      "state": this.dataUpdate.fk_service.stateService,
      "id": this.dataUpdate.fk_service.idService
    }
    this.holydayValue = {
      "uidh": this.dataUpdate.fk_holyday.pkHolyday,
      "descripcionh": this.dataUpdate.fk_holyday.descriptionHolyday,
      "stateh": this.dataUpdate.fk_holyday.stateHolyday,
      "idh": this.dataUpdate.fk_holyday.idHolyday,
      "dateH": this.dataUpdate.fk_holyday.dateHolyday
    }

  }


  /**
  * @date(22-02-2020)
  * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
  * @description Metodo encargado de de validar si la modal que abrira sera de  crear o actualziar,
  * al igual que precargara los datos en caso de que sea  actualizar.
  **/
  validateTypeModal() {
    if (this.dataUpdate != undefined && this.dataUpdate != null && this.dataUpdate != '') {
      this.titleModal = "Actualizar horario especial";
      this.statusActivated = true;
      this.modalUpdate = true;
      this.preloadDataUpdate();
    } else {
      this.titleModal = "Crear nuevo horario especial";
    }
  }
  /**
 * @date(18-02-2020)
 * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
 * @description Metodo encargado de guardar los datos.
 **/
  save() {
   
    if (this.checkFilters(this.modalUpdate)) {
      this.preloader=true
      let modelSpecial = ModelSpecialShedule;
      let id = uuidv4();
      if (this.modalUpdate) {

        const customer = {
          "idCustomer": this.customerValue.id,
          "nameCustomer": this.customerValue.name,
          "stateCustomer": this.customerValue.state,
          "pkCustomer": this.customerValue.uid
        }
        const service = {
          "pkService": this.serviceValue.uid,
          "descriptionService": this.serviceValue.name,
          "stateService": this.serviceValue.state,
          "idService": this.serviceValue.id
        }

        const holyday = {
          "pkHolyday": this.holydayValue.uidh,
          "descriptionHolyday": this.holydayValue.descripcionh,
          "stateHolyday": this.holydayValue.stateh,
          "idHolyday": this.holydayValue.idh,
          "dateHolyday": this.holydayValue.dateH

        }
        modelSpecial.id_Special_Schedule = this.dataUpdate.id_Special_Schedule;
        modelSpecial.pk_special_schedule = this.dataUpdate.pk_special_schedule;
        modelSpecial.fk_customer = customer;
        modelSpecial.fk_service = service;
        modelSpecial.fk_holyday = holyday;
        const date = new Date(holyday.dateHolyday);
        const startArray = this.validateFilters.get('start_time').value.split(':');
        const starTime = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
          startArray[0], startArray[1]);
        modelSpecial.stard_hour_special = starTime;
        const endArray = this.validateFilters.get('end_time').value.split(':');
        const endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
          endArray[0], endArray[1]);
        modelSpecial.end_hour_special = endTime;
        modelSpecial.special_message = this.validateFilters.get('message').value.toUpperCase();
        modelSpecial.status_special_schedule = this.validateFilters.get('state').value;

        this.specialScheduleService.update(modelSpecial).subscribe(resp => {

          this.closemodal();
          this.notificationsService.alert('Aviso', this.message.add, 'success');
        }, error => {
          this.preloader = false;
          this.notificationsService.alert('Aviso', this.message.error, 'error');
        });
      }

      else {

        const customer = {
          "idCustomer": this.customerValue.id,
          "nameCustomer": this.customerValue.name,
          "stateCustomer": this.customerValue.state,
          "pkCustomer": this.customerValue.uid
        }
        const service = {
          "pkService": this.serviceValue.uid,
          "descriptionService": this.serviceValue.name,
          "stateService": this.serviceValue.state,
          "idService": this.serviceValue.id
        }
        const holyday = {
          "pkHolyday": this.holydayValue.uidh,
          "descriptionHolyday": this.holydayValue.descripcionh,
          "stateHolyday": this.holydayValue.stateh,
          "idHolyday": this.holydayValue.idh,
          "dateHolyday": this.holydayValue.dateH
        }
        modelSpecial.id_Special_Schedule = id;
        modelSpecial.pk_special_schedule = id;
        modelSpecial.fk_customer = customer;
        modelSpecial.fk_service = service;
        modelSpecial.fk_holyday = holyday;

        const date = new Date(holyday.dateHolyday);
        const startArray = this.validateFilters.get('start_time').value.split(':');
        const starTime = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
        startArray[0], startArray[1]);
        modelSpecial.stard_hour_special = starTime;

        const endArray = this.validateFilters.get('end_time').value.split(':');
        const endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
          endArray[0], endArray[1]);
        modelSpecial.end_hour_special = endTime;
        modelSpecial.special_message = this.validateFilters.get('message').value.toUpperCase();
        modelSpecial.status_special_schedule = 1;

        this.specialScheduleService.save(modelSpecial).subscribe(resp => {
         
          if (resp.responseCode === 409) {
            this.notificationsService.alert('Aviso', this.message.duplicate, 'warning');
            return;
          }
          this.closemodal();

          this.notificationsService.alert('Aviso', this.message.add, 'success');
          
        }, error => {
         
          if (error.status === 500) {
            this.notificationsService.alert('Aviso', this.message.duplicate, 'warning');
            return;
          }
          this.notificationsService.alert('Aviso', this.message.error, 'error');
        });
      }

    } else {
      this.preloader = false;
      this.notificationsService.alert('Aviso', this.message.mandatory, 'warning');
    }
  }
  /**
   * @date(04-08-2020)
   * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
   * @description Metodo encargado de actualizar la data del usuario
   **/
  update(modelUser: any) {
    
      this.specialScheduleService.update(modelUser).subscribe(resp => {
      if (resp.status) {
        this.notificationsService.alert('Aviso', resp.message, 'success');
        
        this.closemodal();
      }
    }, error => {
       
        this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'error');
      
    });
  }


  onCustomerChange(customer) {
    this.customerValue = customer;
  }

  onServiceChange(service) {
    this.serviceValue = service;
  }

  onMessaje(message) {
    this.validateFilters.get('message').value;
  }
  onHolydayChange(holyday) {
    this.holydayValue = holyday;

  }


  /**
   * @date(22-02-2020)
   * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
   * @description Metodo encargado de obtener todos los servicios  actualmente creados.
   **/

  getDateService() {
    this.serviceService.getAllServiceActive().subscribe(resp => {
      this.dataService = resp.data;
    }, error => {
      this.notificationsService.alert('Aviso', 'Ha ocurrido un error ', 'error')
    });
  }

  /**
   * @date(22-02-2020)
   * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
   * @description Metodo encargado de obtener todos los servicios  actualmente creados.
   **/

  getDateHolyday() {
    this.holydayService.getAllDataHolyday().subscribe(resp => {
      this.dataHolyday = resp.data.filter(festivo => {
           return festivo.dateH >= Date.now();
      });
        if(this.dataHolyday.length < 1){
          this.notificationsService.alert('Aviso', 'No hay fechas registradas en el módulo de Festivos', '');
         } 

    }, error => {
      this.notificationsService.alert('Aviso', 'Ha ocurrido un error ', 'error')
    });
  }



  /**
 * @date(22-02-2020)
 * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
 * @description Metodo encargado de obtener todos los servicios  actualmente creados.
 **/

  getDateCustomer() {
    this.customerService.getAllCustomersActive().subscribe(resp => {
      this.dataCustomer = resp.data;
    }, error => {
      this.notificationsService.alert('Aviso', 'Ha ocurrido un error ', 'error')
    });
  }

  /**
   * @date(24-02-2020)
   * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
   * @description Metodo encargado de verificar si todos los filtros fueron validos.
   **/
  checkFilters(option: boolean): boolean {
    if (option) {
      if (this.validateFilters.valid) {
        return true;
      } else {
        return false;
      }
    } else {
      if (this.validateFilters.get('client').valid &&
        this.validateFilters.get('service').valid &&
        this.validateFilters.get('date').valid &&
        this.validateFilters.get('start_time').valid &&
        this.validateFilters.get('end_time').valid &&
        this.validateFilters.get('message').valid) {

        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  /**
   * @date(22-02-2020)
   * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
   * @description Metodo encargado de cerrar la modal.
  **/
  closemodal() {
    this.dialogRef.close('acept');
  }


}
