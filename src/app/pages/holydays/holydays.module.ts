import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HolydaysRoutingModule } from './holydays-routing.module';
import { ListHolydaysComponent } from './list-holydays/list-holydays.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CreateHolydaysComponent } from './modals/create-holydays/create-holydays.component';
import { DatePipe } from '@angular/common';


//Un Modulo--Directiva
@NgModule({
    declarations: [ListHolydaysComponent, CreateHolydaysComponent,],
    imports: [
        CommonModule,
        HolydaysRoutingModule,
        SharedModule,
    ],
    providers: [DatePipe]
})
export class HolydaysModule { }
