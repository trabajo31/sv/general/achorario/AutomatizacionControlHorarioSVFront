import { Component, OnInit, Inject } from '@angular/core';
import { HolydayService } from 'src/app/services/holydays/holyday.service';
import { UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { NotificationsService } from 'src/app/shared/notifications/notificationsServices';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GlobalMethods } from 'src/app/global/globalmethods';
import { DatePipe } from '@angular/common';
import { ModelHolyday } from '../../config/configh';
import { v4 as uuidv4 } from 'uuid';
import * as moment  from 'moment';
import { MESSAGE_ALERT } from '../../../../global/globalConfig';


@Component({
  selector: 'app-create-holydays',
  templateUrl: './create-holydays.component.html',
  styleUrls: ['./create-holydays.component.css']
})
export class CreateHolydaysComponent implements OnInit {
  //Objeto para controlar filtros.
  public validateFilters: UntypedFormGroup;
  //Objetos que controlan la modal.
  public statusActivated: boolean = false;
  public titleModal: string;
  public filterOptions = this.globalMethods.filterOptions;
  public modalUpdate: boolean = false;
  public message = MESSAGE_ALERT;
  minDate = moment(new Date()).format('YYYY-MM-DD');

  constructor(
    
    private holydayService: HolydayService,
    private fb: UntypedFormBuilder,
    private notificationsService: NotificationsService,
    public dialogRef: MatDialogRef<CreateHolydaysComponent>,
    private globalMethods: GlobalMethods,
    @Inject(MAT_DIALOG_DATA) public dataUpdate: any,
    private datepipe:DatePipe,
  ) { }

  ngOnInit() {
    this.initForm();
    this.validateTypeModal();
  }


  /**
  * @date(17-07-2020)
  * @author Robinson Correa <robinson.correa@grupokonecta.com>
  * @description Metodo encargado de inicializar los filtros.
  **/
  initForm() {
    this.validateFilters = this.fb.group({
      fecha: ['', Validators.required],
      state: ['', Validators.required],
      descripcion:['', [Validators.required, Validators.pattern('^((?!javascript).)*$'), Validators.pattern('^((?!JAVASCRIPT).)*$')]]
    });


  }
  /**
  * @date(17-07-2020)
  * @author Robinson Correa <robinson.correa@grupokonecta.com>
  * @description Metodo encargado de de validar si la modal que abrira sera de  crear o actualziar,
  * al igual que precargara los datos en caso de que sea  actualizar.
  **/
  validateTypeModal() {
    if (this.dataUpdate != undefined && this.dataUpdate != null && this.dataUpdate != '') {
      this.titleModal = "Actualizar Festivo";
      this.statusActivated = true;
      this.modalUpdate = true;
      this.preloadDataUpdate();
    } else {
      this.titleModal = "Crear Nuevo Festivo";
    }
  }


  /**
    * @date(22-02-2020)
    * @author Robinson Correa <robinson.correa@grupokonecta.com>
    * @description Metodo encargado de precargar los datos en la modal de Actualizar.
    **/
  preloadDataUpdate() {
    this.validateFilters.get('descripcion').setValue(this.dataUpdate.descripcionh);
    this.validateFilters.get('state').setValue(this.dataUpdate.stateh);
    this.validateFilters.get('fecha').setValue(this.datepipe.transform(new Date(this.dataUpdate.dateH),'yyyy-MM-dd'));
    this.validateFilters.get('fecha').disable();
 }




 /**
  * @date(04-08-2020)
  * @author Robinson Andres Correa <robinson.correa@grupokonecta.com>
  * @description Metodo encargado de guardar los datos.
  **/
 action() {
  if (this.checkFilters(this.modalUpdate)) {
    let modelHoly = ModelHolyday;

    if (this.modalUpdate) {
      if (this.dataUpdate != null && this.dataUpdate != undefined && this.dataUpdate != '') {
        modelHoly.idh = this.dataUpdate.idh;
        modelHoly.descripcionh = this.validateFilters.get('descripcion').value.toUpperCase();
        modelHoly.dateH = moment(this.validateFilters.get('fecha').value,'YYYY-MM-DD');
        
        modelHoly.stateh = this.validateFilters.get('state').value;
        modelHoly.uidh = this.dataUpdate.uidh;
        this.update(modelHoly);
      } else {
        this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'warning');
      }
    } else {
      var idData = uuidv4();
      modelHoly.idh = idData;
      modelHoly.descripcionh = this.validateFilters.get('descripcion').value.toUpperCase();
      modelHoly.dateH =this.validateFilters.get('fecha').value;
      
      modelHoly.stateh = 1;
      modelHoly.uidh = idData;
      this.save(modelHoly);
    }
  } else {
    this.notificationsService.alert('Aviso', 'Los campos marcados son obligatorios', 'warning');
  }
}










  /**
    * @date(04-08-2020)
    * @author Robinson Correa <robinson.correa@grupokonecta.com>
    * @description Metodo encargado de actualizar los datos.
    **/
 update(modelHoly: any) {
  this.holydayService.update(modelHoly).subscribe(resp => {
    if (resp.status) {
      this.notificationsService.alert('Aviso', resp.message, 'success');
      this.closemodal();
    }
  }, error => {
    this.notificationsService.alert('Aviso', this.message.error, 'error');
  });
}

 /**
    * @date(04-08-2020)
    * @author Robinson Correa <robinson.correa@grupokonecta.com>
    * @description Metodo encargado de guardar los datos.
    **/
save(modelHoly: any) {
  this.holydayService.save(modelHoly).subscribe(resp => {
    if (resp.status) {
      this.notificationsService.alert('Aviso', resp.message, 'success');
      this.closemodal();
    }else
    {
      this.notificationsService.alert('Aviso', resp.message, 'warning');
      this.closemodal();
    }
  }, error => {
    this.notificationsService.alert('Aviso', this.message.error, 'error');
  });
}



  /**
   * @date(17-07-2020)
   * @author Robinson Andres Correa <robinson.correa@grupokonecta.com>
   * @description Metodo encargado de verificar si todos los filtros fueron validos.
   **/
  checkFilters(option: Boolean): Boolean {
    if (option) {
      if (this.validateFilters.valid) {
        return true;
      } else {
        return false;
      }
    } else {
      if (this.validateFilters.get('descripcion').valid && this.validateFilters.get('fecha').valid) {
        return true;
      } else {
        return false;
      }
    }
  }


  /**
    * @date(17-07-2020)
    * @author Robinson Correa <robinson.correa@grupokonecta.com>
    * @description Metodo encargado de cerrar la modal.
    **/
  closemodal() {
    this.dialogRef.close('acept');
  }

}
