import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreateHolydaysComponent } from './create-holydays.component';

describe('CreateHolydaysComponent', () => {
  let component: CreateHolydaysComponent;
  let fixture: ComponentFixture<CreateHolydaysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateHolydaysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateHolydaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
