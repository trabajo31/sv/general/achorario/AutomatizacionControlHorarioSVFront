import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';

import { BreakpointObserver } from '@angular/cdk/layout';
import { HolydayService } from 'src/app/services/holydays/holyday.service';
import { CreateHolydaysComponent } from '../modals/create-holydays/create-holydays.component';
import { NotificationsService } from 'src/app/shared/notifications/notificationsServices';
import { GlobalMethods } from '../../../global/globalmethods';
import { UntypedFormGroup, Validators, UntypedFormBuilder } from '@angular/forms';
import { ManagePermissions } from 'src/app/shared/validators/managePermissions';
import { NameModules } from '../../../model/permits.model';
import { MESSAGE_ALERT } from '../../../global/globalConfig';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-list-holydays',
  templateUrl: './list-holydays.component.html',
  styleUrls: ['./list-holydays.component.css']
})
export class ListHolydaysComponent implements OnInit {

  preloader: boolean;
  // Objetos encargados de paginar y organizar la tabla.
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  // Objeto para agrupar filtros
  public validateFilters: UntypedFormGroup;
  //Objeto con columnas de la tabla.
  displayedColumns = ['fecha', 'descripcion', 'status', 'action'];
  // Data de la tabla de registros
  dataSource = new MatTableDataSource<any>();
  // Objeto para controlar la data de envio.
  private objectDataDelete = {
    id: 0,
    name: "",
    state: 0,
    uid: 0,
  }

  //Mensajes de alerta
  public message = MESSAGE_ALERT;

  dataPermission: any;
  nameModules = NameModules;

  constructor(breakpointObserver: BreakpointObserver,
    private HolydayService: HolydayService,
    private dialog: MatDialog,
    private notificationsService: NotificationsService,
    private globalMethods: GlobalMethods,
    private formBuilder: UntypedFormBuilder,
    private managePermissions: ManagePermissions,
    private datepipe:DatePipe) {
    let columns = ['fecha', 'descripcion', 'status', 'action'];
    breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
      this.displayedColumns = columns;
    });
  }





  ngOnInit() {
    this.initForm();
    this.getData();
    this.managePermissions.getPermitsIndividual(NameModules.holydays);
  }

  /**
 * @date(16-07-2020)
 * @author Robinson Andres Correa<robinson.correa@grupokonecta.com>
 * @description Metodo encargado de inicializar el campo de busqueda
 **/
  initForm() {
    this.validateFilters = this.formBuilder.group({
      search: ['', [Validators.required, Validators.pattern('[a-zA-ZÑáéíóú ]*')]]
    });
  }


  getData() {
    this.preloader = true;
    this.HolydayService.getAllService().subscribe(resp => {
      if (resp.data != undefined && resp.data != null) {
        this.dataSource = new MatTableDataSource(resp.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.changeFilterConditions();
      }
      this.preloader=false;
    }, error => {
      this.preloader=false;
      this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'error');
    });
  }

  /**
    * @date(16-07-2020)
    * @author Robinson Andres Correa<robinson.correa@grupokonecta.com>
    * @description Metodo encargado de eliminar el registro
    * @param data  data a eliminar
    * 
    **/
  deleteSerivece(data: any = null) {
    if (data != null) {
      this.HolydayService.delete(data).subscribe(resp => {
        this.notificationsService.alert('Aviso', 'Registro eliminado correctamente', 'success');
        this.cleanSearchEngine();
        this.getData();
      }, error => {
        this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'error');
      });
    }
  }

  /**
  * @date(16-07-2020)
  * @author Robinson Andres Correa <robinson.correa@grupokonecta.com>
  * @description Metodo encargado de abrir la modal para crear y actualizar filtro.
  * @param dataModal  objeto que contendra la data del registro seleccionado
  **/
  openModal(dataModal = null) {
    this.dataPermission = this.managePermissions.getModulePermissions();
    if (this.dataPermission.permission === 1) {
      this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'warning');
    } else if (this.dataPermission.permission === 2) {
      let dialogRef;
      dialogRef = this.dialog.open(CreateHolydaysComponent, {
        data: dataModal,
        height: '400px',
        width: '600px',
        autoFocus: false,
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result === 'acept') {
          this.ngOnInit();
        }
      });
    } else {
      this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'warning');
    }
  }


  /**
    * @date(17-07-2020)
    * @author Robinson Andres Correa <robinson.correa@grupokonecta.com>
    * @description Metodo encargado de abrir la modal para crear y actualizar filtro.
    * @param event  objeto que contendra el evento para activar la busqueda.
    **/
  filterTable(event: Event) {
    const filter = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filter.trim().toLowerCase();
  }

  changeFilterConditions() {
    this.dataSource.filterPredicate = (data, filter) => {
      if (data.descripcionh.toLowerCase().indexOf(filter) >= 0) {
        return true;
      }
      const date = this.datepipe.transform(data.dateH, 'dd/MM/yyyy').toString();
      if (date.indexOf(filter) >= 0) {
        return true;
      }
      return false;
    };
  }

  /**
    * @date(17-07-2020)
    * @author Robinson Andres Correa <robinson.correa@grupokonecta.com>
  * @description Metodo encargado de limpiar el campo de busqueda
  **/
  cleanSearchEngine() {
    if (this.validateFilters.get('search').value != '') {
      this.validateFilters.get('search').setValue('');
    }
  }

}
