import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListHolydaysComponent } from './list-holydays.component';

describe('ListHolydaysComponent', () => {
  let component: ListHolydaysComponent;
  let fixture: ComponentFixture<ListHolydaysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListHolydaysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListHolydaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
