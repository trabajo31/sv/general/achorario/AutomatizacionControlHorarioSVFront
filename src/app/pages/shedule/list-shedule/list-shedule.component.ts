import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { BreakpointObserver } from '@angular/cdk/layout';
import { NotificationsService } from 'src/app/shared/notifications/notificationsServices';
import { GlobalMethods } from 'src/app/global/globalmethods';
import { SheduleService } from 'src/app/services/shedule/shedule.service';
import { CreateSheduleComponent } from '../modals/create-shedule/create-shedule.component';
import { UpdateScheduleComponent } from '../modals/update-schedule/update-schedule.component';
import { DeleteScheduleComponent } from '../modals/delete-schedule/delete-schedule.component';
import { AlertService } from 'src/app/services/Useful/alert-service';
import { ManagePermissions } from 'src/app/shared/validators/managePermissions';
import { NameModules } from '../../../model/permits.model';
import { shedule } from '../config/config';
import { SelectionModel } from '@angular/cdk/collections';
import { specialShedule } from '../../specialSchedule/config/config';

import { MESSAGE_ALERT } from '../../../global/globalConfig';
@Component({
  selector: 'app-list-shedule',
  templateUrl: './list-shedule.component.html',
  styleUrls: ['./list-shedule.component.css']
})
export class ListSheduleComponent implements OnInit {

  preloader:boolean;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  // Objeto para agrupar filtros
  public validateFilters: UntypedFormGroup;
  //Objeto con columnas de la tabla.

  // Objeto para controlar la data de envio.
  dataPermission: any;
  nameModules = NameModules;
  displayedColumns = ['select', 'client', 'service', 'date', 'start_time', 'end_time', 'status', 'actions'];
  // Data de la tabla de registros
  selection = new SelectionModel<shedule>(true, []);
  dataSource = new MatTableDataSource([]);
  // Objeto para controlar la data de envio.
  idspecialSelected = [];
  checkboxOptions = [{ value: 'disable', name: 'desactivar estados', icon: 'not_interested', color: 'warn' }];

  //Mensajes de alerta
  public message = MESSAGE_ALERT;


  constructor(breakpointObserver: BreakpointObserver,
    private scheduleService: SheduleService,
    private dialog: MatDialog,
    private alertService: AlertService,
    private managePermissions: ManagePermissions,
    private notificationsService: NotificationsService,
    private globalMethods: GlobalMethods,
    private formBuilder: UntypedFormBuilder
  ) {
    let columns = ['select', 'client', 'service', 'date', 'start_time', 'end_time', 'status', 'actions'];
    breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
      this.displayedColumns = columns;
    });
  }

  ngOnInit() {
    this.initForm();
    this.getData();
    this.managePermissions.getPermitsIndividual(NameModules.shelude);
  }

  /**
 * @date(12-07-2020)
 * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
 * @description Metodo encargado de inicializar el campo de busqueda
 **/
  initForm() {
    this.validateFilters = this.formBuilder.group({
      search: ['', [Validators.required, Validators.pattern('[a-zA-ZÑáéíóú ]*')]]
    });
  }

  DAYS = [
    {
      value: "lectura",
      label: "Lectura"
    },
    {
      value: "lectura_escritura",
      label: "Lectura y Escritura"
    }
  ];


  getData() {
    this.preloader = true;
    this.scheduleService.getAllService().subscribe(resp => {
      if (resp != undefined && resp != null) {
        this.dataSource = new MatTableDataSource(resp.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.changeFilterConditions();
      }
      this.preloader = false;
    }, _error => {
      this.preloader = false;
      this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'error');
    });
  }

  /**
    * @date(22-08-2020)
    * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
    * @description Metodo encargado de eliminar
    * @param idSpecialSchedule  Variable contenedora.
  **/
  disableStatusShedule() {
    this.alertService.confirm(
      '¿Está seguro desactivar el estado horario especial',
      '',
      'warning'
      
    ).then(res => {
      if (res) {
        let selecionados = [];
        const numSelected = this.selection.selected;
        if (numSelected.length > 0) {
          numSelected.map(
            selected => selecionados.push({ id: selected.idServiceSchedule })
          );
        }
        this.dataSource = new MatTableDataSource<specialShedule>([]);
        this.scheduleService.deleteShedule(selecionados).subscribe(resp => {
            if (resp.status) {
              this.getData();
              this.notificationsService.alert('', resp.message, 'success');
            } else {
              this.notificationsService.alert('', resp.message, 'error');
            }
            this.getData();
          }), _error => {
            this.getData();
          };
      }
    });
  }


 /**
   * @date(18-07-2020)
   * @author Jesús David Sanchez Pabón <jesus.sanchezgrupokonecta.com>
   * @description Metodo encargado selecionar en checkbox.
   */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;

    return numSelected === numRows;
  }
   /**
   * @date(18-07-2020)
   * @author Jesús David Sanchez Pabón <jesus.sanchezgrupokonecta.com>
   * @description metodo encargado realizar las acciones del inactivo
   * @param action 
   */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

   /**
   * @date(18-07-2020)
   * @author Jesús David Sanchez Pabón <jesus.sanchezgrupokonecta.com>
   * @description metodo encargado realizar las acciones del inactivo
   * @param action 
   */
  actionCasesSelected(action) {

    this.idspecialSelected = [];

    for (const i in this.selection.selected) {
      if (this.selection.selected.hasOwnProperty(i)) {
        this.idspecialSelected.push(this.selection.selected[i]['id_Special_Schedule']);
      }

    }
    switch (action) {
      case 'disable':
        this.disableStatusShedule();
        break;
      default:
        break;
    }
  }


  /**
* @date(10-07-2020)
* @author Jesus David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
* @description Metodo encargado de abrir la modal para crear y actualizar filtro.
* @param dataModal  objeto que contendra la data del registro seleccionado
**/
  openModal(dataModal = null) {
    this.dataPermission = this.managePermissions.getModulePermissions();
    if (this.dataPermission.permission === 1) {
      this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'warning');
    } else if (this.dataPermission.permission === 2) {
      let dialogRef;
      dialogRef = this.dialog.open(CreateSheduleComponent, {
        data: dataModal,
        height: '600px',
        width: '800px',
        autoFocus: false,
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result === 'acept') {
          this.ngOnInit();
        }
      });
    } else {
      this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'warning');
    }
  }

  /**
  * @date(10-07-2020)
  * @author Jesus David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
  * @description Metodo encargado de abrir la modal para crear y actualizar filtro.
  * @param dataModal  objeto que contendra la data del registro seleccionado
  **/
  openModalUpdate(dataModal = null) {
    let dialogRef;
    dialogRef = this.dialog.open(UpdateScheduleComponent, {
      data: dataModal,
      height: '600px',
      width: '800px',
      autoFocus: false,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'acept') {
        this.ngOnInit();
      }
    });
  }

   /**
* @date(10-07-2020)
* @author Jesus David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
* @description Metodo encargado de abrir la modal para crear y actualizar filtro.
* @param dataModal  objeto que contendra la data del registro seleccionado
**/
openModalDelete(dataModal = null) {
  let dialogRef;
  dialogRef = this.dialog.open(DeleteScheduleComponent, {
    data: dataModal,
    height: '500px',
    width: '800px',
    autoFocus: false,
    disableClose: true
  });
  dialogRef.afterClosed().subscribe(result => {
    if (result === 'acept') {
      this.ngOnInit();
    }
  });
}

  /**
* @date(18-07-2020)
* @author Jesús David Sanchez Pabón <jesus.sanchezgrupokonecta.com>
* @description Metodo encargado de abrir la modal para crear y actualizar filtro.
* @param event  objeto que contendra el evento para activar la busqueda.
**/
  filterTable(event: Event) {
    const filter = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filter.trim().toLowerCase();
  }

  changeFilterConditions() {
    this.dataSource.filterPredicate = (data, filter) => {
      if (data.fkCustomer.nameCustomer.toLowerCase().indexOf(filter) >= 0) {
        return true;
      }
      return false;
    };
  }


  /**
* @date(18-07-2020)
* @author Jesús David Sanchez Pabón <jesus.sanchezgrupokonecta.com>
* @description Metodo encargado de limpiar el campo de busqueda
**/
  cleanSearchEngine() {
    if (this.validateFilters.get('search').value != '') {
      this.validateFilters.get('search').setValue('');
    }
  }
}
