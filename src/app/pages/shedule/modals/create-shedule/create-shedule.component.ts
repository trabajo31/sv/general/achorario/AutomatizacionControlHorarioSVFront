import { Component, OnInit, Inject } from '@angular/core';
import { UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { NotificationsService } from 'src/app/shared/notifications/notificationsServices';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GlobalMethods } from 'src/app/global/globalmethods';
import { DatePipe } from '@angular/common';
import { ServiceService } from 'src/app/services/service/service.service';
import { ModelShedule } from '../../config/config';
import { SheduleService } from 'src/app/services/shedule/shedule.service';
import { CustomerService } from 'src/app/services/customers/customer.service';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-create-shedule',
  templateUrl: './create-shedule.component.html',
  styleUrls: ['./create-shedule.component.css']
})
export class CreateSheduleComponent implements OnInit {

  //Objeto para controlar filtros.
  validateFilters: UntypedFormGroup;
  //Objetos que controlan la modal.
  statusActivated: boolean = false;
  titleModal: string;
  public filterOptions = this.globalMethods.filterOptions;
  private modalUpdate: boolean = false;
  //Objeto para el control de datos de envio
  dataCustomer: [];
  dataService: [];


  private customerValue: any;
  //Objeto para el control de datos de envio
  private serviceValue: any;

  private objectDataUpdate = {
    pk_special_schedule: 0,
    id_Special_Schedule: 0,
    fk_customer: {},
    fk_service: {},
    fk_holyday: {},
    stard_hour_special: new Date(),
    end_hour_special: new Date(),
    special_message: "",
    status_special_schedule: 0
  }

  constructor(
    private scheduleService: SheduleService,
    private serviceService: ServiceService,
    private customerService: CustomerService,
    private fb: UntypedFormBuilder,
    private notificationsService: NotificationsService,
    public dialogRef: MatDialogRef<CreateSheduleComponent>,
    private globalMethods: GlobalMethods,
    private datePipe: DatePipe,
    @Inject(MAT_DIALOG_DATA) public dataUpdate: any,
  ) {
    this.getDateCustomer();
    this.getDateService();
  }
  ngOnInit() {
    this.initForm();
    this.validateTypeModal();
  }
  selectClient(c1, c2) {

    if (c1.id === c2.idCustomer) {
      this.customerValue = c1;
      return true;
    }
    return false;
  }

  selectService(c1, c2) {

    if (c1.id === c2.idService) {
      this.serviceValue = c1;
      return true;
    }
    return false;
  }

  /**
  * @date(18-02-2020)
  * @author  Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
  * @description Metodo encargado de inicializar los filtros.
  **/
  initForm() {
    this.validateFilters = this.fb.group({
      client: ['', Validators.required],
      service: ['', Validators.required],
      start_time: ['', Validators.required],
      end_time: ['', Validators.required],
      start_timeSaturday: ['', Validators.required],
      end_timeSaturday: ['', Validators.required],
      start_timeSunday: ['', Validators.required],
      end_timeSunday: ['', Validators.required],
      message: ['', [Validators.required, Validators.pattern('^((?!javascript).)*$'), Validators.pattern('^((?!JAVASCRIPT).)*$')]],
      state: ['', Validators.required, ]

    });

  }

  /**
  * @date(18-02-2020)
  * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
  * @description Metodo encargado de precargar los datos en la modal de Actualizar.
  **/
  preloadDataUpdate() {
    this.customerValue = {
      "id": this.dataUpdate.fkCustomer.idCustomer,
      "name": this.dataUpdate.fkCustomer.nameCustomer,
      "state": this.dataUpdate.fkCustomer.stateCustomer,
      "uid": this.dataUpdate.fkCustomer.pkCustomer

    }
    this.serviceValue = {
      "uid": this.dataUpdate.fkService.pkService,
      "name": this.dataUpdate.fkService.descriptionService,
      "state": this.dataUpdate.fkService.stateService,
      "id": this.dataUpdate.fkService.idService
    }
    this.validateFilters.get('client').setValue(this.customerValue.name);
    this.validateFilters.get('service').setValue(this.serviceValue.name);
    this.validateFilters.get('date').setValue(this.dataUpdate.idDay);

    let time = this.datePipe.transform(new Date(this.dataUpdate.startHour), 'hh:mm');
    this.validateFilters.get('start_time').setValue(time);

    time = this.datePipe.transform(new Date(this.dataUpdate.endHour), 'hh:mm');
    this.validateFilters.get('end_time').setValue(time);
    this.validateFilters.get('message').setValue(this.dataUpdate.messageSchedule);
    this.validateFilters.get('state').setValue(this.dataUpdate.scheduleState);
  }


  /**
  * @date(22-02-2020)
  * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
  * @description Metodo encargado de de validar si la modal que abrira sera de  crear o actualziar,
  * al igual que precargara los datos en caso de que sea  actualizar.
  **/
  validateTypeModal() {
    if (this.dataUpdate != undefined && this.dataUpdate != null && this.dataUpdate != '') {
      this.titleModal = "Actualizar horario";
      this.statusActivated = true;
      this.modalUpdate = true;
      this.preloadDataUpdate();
    } else {
      this.titleModal = "Crear nuevo horario";
    }
  }
  /**
 * @date(18-02-2020)
 * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
 * @description Metodo encargado de guardar los datos.
 **/

  save() {
    if (this.checkFilters(this.modalUpdate)) {
      let modelSpecial = ModelShedule;
      let id = uuidv4();
      if (this.modalUpdate) {

        const customer = {
          "idCustomer": this.customerValue.id,
          "nameCustomer": this.customerValue.name,
          "stateCustomer": this.customerValue.state,
          "pkCustomer": this.customerValue.uid
        }
        const service = {
          "pkService": this.serviceValue.uid,
          "descriptionService": this.serviceValue.name,
          "stateService": this.serviceValue.state,
          "idService": this.serviceValue.id
        }
        modelSpecial.idServiceSchedule = this.dataUpdate.idServiceSchedule;
        modelSpecial.pkServiceSchedule = this.dataUpdate.pkServiceSchedule;
        modelSpecial.fkCustomer = customer;
        modelSpecial.fkService = service;
        modelSpecial.idDay = 1;
        modelSpecial.idDaySaturday = 6;
        modelSpecial.idDaySunday = 7;
        const date = new Date();
        const startArray = this.validateFilters.get('start_time').value.split(':');
        const starTime = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
          startArray[0], startArray[1]);
        modelSpecial.startHour = starTime;
        const endArray = this.validateFilters.get('end_time').value.split(':');
        const endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
          endArray[0], endArray[1]);
        modelSpecial.endHour = endTime;

        const startArraySaturday = this.validateFilters.get('start_timeSaturday').value.split(':');
        const starTimeSaturday = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
          startArraySaturday[0], startArraySaturday[1]);
        modelSpecial.startHourSaturday = starTimeSaturday;
        const endArraySaturday = this.validateFilters.get('end_timeSaturday').value.split(':');
        const endTimeSaturday = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
          endArraySaturday[0], endArraySaturday[1]);
        modelSpecial.endHourSaturday = endTimeSaturday;

        const startArraySunday = this.validateFilters.get('start_timeSunday').value.split(':');
        const starTimeSunday = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
          startArraySunday[0], startArraySunday[1]);
        modelSpecial.startHourSunday = starTimeSunday;
        const endArraySunday = this.validateFilters.get('end_timeSunday').value.split(':');
        const endTimeSunday = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
          endArraySunday[0], endArraySunday[1]);
        modelSpecial.endHourSunday = endTimeSunday;

        modelSpecial.messageSchedule = this.validateFilters.get('message').value;
        modelSpecial.scheduleState = this.validateFilters.get('state').value;
        this.scheduleService.update(modelSpecial).subscribe(resp => {
          this.closemodal();
          this.notificationsService.alert('Aviso', 'Datos actualizado correctamente', 'success');
        }, error => {
          this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'error');
        });
      }
      else {

        const customer = {
          "idCustomer": this.customerValue.id,
          "nameCustomer": this.customerValue.name,
          "stateCustomer": this.customerValue.state,
          "pkCustomer": this.customerValue.uid
        }
        const service = {
          "pkService": this.serviceValue.uid,
          "descriptionService": this.serviceValue.name,
          "stateService": this.serviceValue.state,
          "idService": this.serviceValue.id
        }

        modelSpecial.idServiceSchedule = id;
        modelSpecial.pkServiceSchedule = id;
        modelSpecial.fkCustomer = customer;
        modelSpecial.fkService = service;
        modelSpecial.idDay = 1;
        modelSpecial.idDaySaturday = 6;
        modelSpecial.idDaySunday = 7;
        const date = new Date();
        const startArray = this.validateFilters.get('start_time').value.split(':');
        const starTime = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
          startArray[0], startArray[1]);
        modelSpecial.startHour = starTime;

        const endArray = this.validateFilters.get('end_time').value.split(':');
        const endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
          endArray[0], endArray[1]);
        modelSpecial.endHour = endTime;

        const startArraySaturday = this.validateFilters.get('start_timeSaturday').value.split(':');
        const starTimeSaturday = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
          startArraySaturday[0], startArraySaturday[1]);
        modelSpecial.startHourSaturday = starTimeSaturday;
        const endArraySaturday = this.validateFilters.get('end_timeSaturday').value.split(':');
        const endTimeSaturday = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
          endArraySaturday[0], endArraySaturday[1]);
        modelSpecial.endHourSaturday = endTimeSaturday;

        const startArraySunday = this.validateFilters.get('start_timeSunday').value.split(':');
        const starTimeSunday = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
          startArraySunday[0], startArraySunday[1]);
        modelSpecial.startHourSunday = starTimeSunday;
        const endArraySunday = this.validateFilters.get('end_timeSunday').value.split(':');
        const endTimeSunday = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
          endArraySunday[0], endArraySunday[1]);
        modelSpecial.endHourSunday = endTimeSunday;

        modelSpecial.messageSchedule = this.validateFilters.get('message').value;
        modelSpecial.scheduleState = 1;
        this.scheduleService.save(modelSpecial).subscribe(resp => {
          if (resp.responseCode == 409) {
            this.closemodal();
            this.notificationsService.alert('Aviso', 'Registro Previemente Realizado', 'warning');
          } else {
            this.closemodal();
            this.notificationsService.alert('Aviso', 'Datos almacenados correctamente', 'success');
          }
        }, error => {
          this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'error');
        });
      }
    } else {
      this.notificationsService.alert('Aviso', 'Los campos marcados son obligatorios', 'warning');
    }
  }

  onCustomerChange(customer) {
    this.customerValue = customer;


  }

  onServiceChange(service) {
    this.serviceValue = service;


  }

  onMessaje(message) {
    this.validateFilters.get('message').value;
  }


  /**
 * @date(22-02-2020)
 * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
 * @description Metodo encargado de obtener todos los servicios  actualmente creados.
 **/

  getDateService() {
    this.serviceService.getAllServiceActive().subscribe(resp => {
      this.dataService = resp.data;

    }, _error => {
      this.notificationsService.alert('Aviso', 'Ha ocurrido un error ', 'error');
    });
  }

  /**
 * @date(22-02-2020)
 * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
 * @description Metodo encargado de obtener todos los servicios  actualmente creados.
 **/

  getDateCustomer() {
    this.customerService.getAllCustomersActive().subscribe(resp => {
      this.dataCustomer = resp.data;
    }, error => {
      this.notificationsService.alert('Aviso', 'Ha ocurrido un error ', 'error')
    });
  }
  /**
   * @date(24-02-2020)
   * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
   * @description Metodo encargado de verificar si todos los filtros fueron validos.
   **/
  checkFilters(option: boolean): boolean {
    if (option) {
      if (this.validateFilters.valid) {
        return true;
      } else {
        return false;
      }
    } else {
      if (this.validateFilters.get('client').valid &&
        this.validateFilters.get('service').valid &&
        this.validateFilters.get('start_time').valid &&
        this.validateFilters.get('end_time').valid &&
        this.validateFilters.get('start_timeSaturday').valid &&
        this.validateFilters.get('end_timeSaturday').valid &&
        this.validateFilters.get('start_timeSunday').valid &&
        this.validateFilters.get('end_timeSunday').valid &&
        this.validateFilters.get('message').valid) {

        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  /**
   * @date(22-02-2020)
   * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
   * @description Metodo encargado de cerrar la modal.
  **/
  closemodal() {
    this.dialogRef.close('acept');
  }

}
