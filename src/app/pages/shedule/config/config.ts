export let ModelShedule = {

    pkServiceSchedule: 0,
    idServiceSchedule: 0,
    fkCustomer: {},
    fkService: {},
    idDay: 0,
    startHour: new Date(),
    endHour: new Date(),
    idDaySaturday: 0,
    startHourSaturday: new Date(),
    endHourSaturday: new Date(),
    idDaySunday: 0,
    startHourSunday: new Date(),
    endHourSunday: new Date(),
    messageSchedule: "",
    scheduleState: 0
}

export interface shedule {


    pkServiceSchedule: String,
    idServiceSchedule: String,
    fkCustomer: String,
    fkService: String,
    idDay: 0,
    startHour:String,
    endHour: String,
    idDaySaturday: 0,
    startHourSaturday:String,
    endHourSaturday:String,
    idDaySunday: 0,
    startHourSunday:String,
    endHourSunday:String,
    messageSchedule:String,
    scheduleState: 0
}


