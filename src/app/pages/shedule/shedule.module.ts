import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { SheduleRoutingModule } from './shedule-routing.module';
import { CreateSheduleComponent } from './modals/create-shedule/create-shedule.component';
import { ListSheduleComponent } from './list-shedule/list-shedule.component';
import { UpdateScheduleComponent } from './modals/update-schedule/update-schedule.component';
import { DeleteScheduleComponent } from './modals/delete-schedule/delete-schedule.component';


@NgModule({
    declarations: [CreateSheduleComponent, ListSheduleComponent, UpdateScheduleComponent, DeleteScheduleComponent],
    imports: [
        CommonModule,
        SharedModule,
        SheduleRoutingModule
    ],
    providers: [DatePipe]
})
export class SheduleModule { }
