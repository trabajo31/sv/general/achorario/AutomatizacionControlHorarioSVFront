import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListCountriesComponent } from './list-countries/list-countries.component';
import { SharedModule } from '../../shared/shared.module';
import { CreateCountryComponent } from './modals/create-country/create-country.component';
import { CountriesRoutingModule } from './countries-routing.module';
import { OnlyLettersDirective } from 'src/app/directives/only-letters.directive';



@NgModule({
    declarations: [ListCountriesComponent, CreateCountryComponent, OnlyLettersDirective],
    imports: [
        CommonModule,
        SharedModule,
        CountriesRoutingModule
    ]
})
export class CountriesModule { }
