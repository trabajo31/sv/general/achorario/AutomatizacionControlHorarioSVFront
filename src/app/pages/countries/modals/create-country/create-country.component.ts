import { Component, OnInit, Inject } from '@angular/core';
import { UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { GlobalMethods } from '../../../../global/globalmethods';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { v4 as uuidv4 } from 'uuid';
import { CountriesService } from '../../../../services/countries/countries.service';
import { NotificationsService } from '../../../../shared/notifications/notificationsServices';
import { 
  virtualServiceValidator, validateXSS 
} from '../../../../services/utils/directives/form-validations.directive';
import { MESSAGE_ALERT } from '../../../../global/globalConfig';

@Component({
  selector: 'app-create-country',
  templateUrl: './create-country.component.html',
  styleUrls: ['./create-country.component.css']
})
export class CreateCountryComponent implements OnInit {

  public validateFilters: UntypedFormGroup;
  preloader: boolean = false;
  public statusActivated: Boolean;
  public titleModal: String;
  public filterOptions;
  private modalUpdate: Boolean;

  private objectDataUpdate = {
    id: 0,
    name: '',
    state: 0,
    uid: 0
  };

  public clienValidate = 'país';
  public message = MESSAGE_ALERT;

  constructor(
    private globalMethods: GlobalMethods,
    private formBuilder: UntypedFormBuilder,
    private notificationsService: NotificationsService,
    public dialogRef: MatDialogRef<CreateCountryComponent>,
    private countriesService: CountriesService,
    @Inject(MAT_DIALOG_DATA) public dataUpdate
  ) {
    this.statusActivated = false;
    this.filterOptions = this.globalMethods.filterOptions;
  }

  ngOnInit() {
    this.initForm();
    this.validateTypeModal();
  }

  initForm() {
    this.validateFilters = this.formBuilder.group({
      country: ['', [Validators.required, Validators.maxLength(50), 
        virtualServiceValidator(this.clienValidate), validateXSS()]],
      state: ['', [Validators.required]]
    });
  }

  /* inputUpper(formControlName, event) {
    this.validateFilters.get(formControlName).setValue(event.target.value.toUpperCase());

  } */

  /**
   * @date(25-09-2020)
   * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
   * @description Funcion para validar el tipo de modal si es update o nuevo registro
   **/
  validateTypeModal() {
    if (this.dataUpdate !== undefined && this.dataUpdate !== null && this.dataUpdate !== '') {
      this.titleModal = 'Actualizar país';
      this.statusActivated = true;
      this.modalUpdate = true;
      this.preloadDataUpdate();
    } else {
      this.titleModal = 'Agregar nuevo país';
    }
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para precargar el formulario
 **/
  preloadDataUpdate() {
    this.validateFilters.get('country').setValue(this.dataUpdate.name);
    this.validateFilters.get('state').setValue(this.dataUpdate.state);
  }


  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para guardar o editar registro
 **/
  save() {
    if (this.checkFilters(this.modalUpdate)) {
      this.preloader = true;
      const params = this.objectDataUpdate;
      if (this.modalUpdate) {
        params.id = this.dataUpdate.id;
        params.name = this.validateFilters.get('country').value.toUpperCase();
        
        params.state = this.validateFilters.get('state').value;
        params.uid = this.dataUpdate.uid;
        this.updateCountry(params);
      } else {
        this.preloader = false;
        const idData = uuidv4();
        params.id = idData;
        params.name = this.validateFilters.get('country').value.toUpperCase();
        params.state = 1;
        params.uid = idData;
        this.saveCountry(params);
        
      }
    } else {
      this.preloader = false;
      this.notificationsService.alert('Aviso', this.message.mandatory, 'warning');
    }
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para guardar un registro en caso de que sea uno nuevo
 **/
  saveCountry(params) {
    this.countriesService.save(params).subscribe(resp => {
      if (resp.responseCode === 400) {
        this.notificationsService.alert('Aviso', 'Registro fue previamente realizado', 'warning');
        return;
      }
      this.closemodal();
      this.notificationsService.alert('Aviso', 'Datos almacenados correctamente', 'success');
    }, () => {
      this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'error');
    });
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para actualizar un registro
 **/
  updateCountry(params) {
    this.countriesService.update(params).subscribe(() => {
      this.closemodal();
      this.notificationsService.alert('Aviso', 'Datos actualizados correctamente', 'success');
    }, () => {
      this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'error');
    });
  }

  checkFilters(option: Boolean): Boolean {
    if (option) {
      if (this.validateFilters.valid) {
        return true;
      } else {
        return false;
      }
    } else {
      if (this.validateFilters.get('country').valid) {
        return true;
      } else {
        return false;
      }
    }
  }

  closemodal() {
    this.dialogRef.close('acept');
  }

  get formValidator() {
    return this.validateFilters.controls; 
  }

}
