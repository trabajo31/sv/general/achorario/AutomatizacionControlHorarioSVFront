import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BreakpointObserver } from '@angular/cdk/layout';
import { MatTableDataSource } from '@angular/material/table';
import { UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { CountriesService } from '../../../services/countries/countries.service';
import { CreateCountryComponent } from '../modals/create-country/create-country.component';
import { GlobalMethods } from '../../../global/globalmethods';
import { NotificationsService } from '../../../shared/notifications/notificationsServices';
import { ManagePermissions } from '../../../shared/validators/managePermissions';
import { NameModules } from '../../../model/permits.model';
import { MESSAGE_ALERT } from '../../../global/globalConfig';


@Component({
  selector: 'app-list-countries',
  templateUrl: './list-countries.component.html',
  styleUrls: ['./list-countries.component.css']
})
export class ListCountriesComponent implements OnInit {

  // Objetos para paginar y ordenar la tabla
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  // agrupar filtros
  public validateFilters: UntypedFormGroup;
  //Mensajes de alerta
  public message = MESSAGE_ALERT;

  preloader: boolean = false;

  // Columnas de la tabla
  displayedColumns = ['country', 'status', 'action'];
  dataPermission: any;
  nameModules = NameModules;

  dataSource: MatTableDataSource<any>;

  private objectDataDelete = {
    id: 0,
    name: '',
    state: 0,
    uid: 0
  };

  constructor(
    private breakPointObserver: BreakpointObserver,
    private coutriesService: CountriesService,
    private formBuilder: UntypedFormBuilder,
    private globalMethods: GlobalMethods,
    private notificationsService: NotificationsService,
    private dialog: MatDialog,
    private managePermissions: ManagePermissions
  ) {
    const columns = ['country', 'status', 'action'];
    this.breakPointObserver.observe(['(max-width: 600px)'])
      .subscribe(() => this.displayedColumns = columns);
  }

  ngOnInit() {
    this.initForm();
    this.getData();
    this.managePermissions.getPermitsIndividual(NameModules.countries);
  }

  initForm() {
    this.validateFilters = this.formBuilder.group({
      search: ['', [Validators.required, Validators.pattern('[a-zA-ZÑáéíóú ]*')]]
    });
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para obtener la data en la datasource de la tabla
 **/
  getData() {
    this.preloader = true;
    this.coutriesService.getAllCountries().subscribe(resp => {
      if (resp.data !== undefined && resp.data !== null) {
        this.dataSource = new MatTableDataSource(resp.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
      this.preloader =false;
    });
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para eliminar un registro
 **/
  deleteCountry(data: any = null) {
    if (data !== null) {
      this.coutriesService.delete(data).subscribe(() => {
        this.notificationsService.alert('Aviso', 'Registro eliminado correctamente', 'success');
        this.cleanSearchEngine();
        this.getData();
      }, () => {
        this.notificationsService.alert('Aviso', 'Ha ocurrido un error', 'error');
      });
    }
  }

  /**
 * @date(25-09-2020)
 * @author Daniel Mejia Benjumea<daniel.mejia@grupokonecta.com>
 * @description Funcion para abrir la modal de editar o agregar un nuevo país
 **/
  openModal(dataModal = null) {
    this.dataPermission = this.managePermissions.getModulePermissions();
    if (this.dataPermission.permission === 1) {
      this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'warning');
    } else if (this.dataPermission.permission === 2) {
      const dialogRef = this.dialog.open(CreateCountryComponent, {
        data: dataModal,
        width: '600px',
        autoFocus: false,
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result === 'acept') {
          this.ngOnInit();
        }
      });
    } else {
      this.notificationsService.alert('Aviso', this.message.invalid_permissions, 'warning');
    }
  }

  filterTable(event: Event) {
    const filter = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filter.trim().toLowerCase();
  }

  cleanSearchEngine() {
    if (this.validateFilters.get('search').value !== '') {
      this.validateFilters.get('search').setValue('');
    }
  }
}
