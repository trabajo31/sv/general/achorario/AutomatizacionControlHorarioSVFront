/*=================================================
                NOMBRE DE MODULOS
===================================================*/

export const NameModules = {
    countries: "2",
    customers: "3",
    holydayByCountry: "1",
    holydays: "4",
    rol: "5",
    service: "6",
    shelude: "7",
    specialShedule: "8",
    moduleByRol: "10",
    user: "9"
};

/*=================================================
                 OBJETO DE MODULO
===================================================*/
export let Modules = {
    countries: {
        module: "",
        permission: "",
        visible: false
    },
    customers: {
        module: "",
        permission: "",
        visible: false
    },
    holydayByCountry: {
        module: "",
        permission: "",
        visible: false
    },
    holydays: {
        module: "",
        permission: "",
        visible: false
    },
    rol: {
        module: "",
        permission: "",
        visible: false
    },
    service: {
        module: "",
        permission: "",
        visible: false
    },
    shelude: {
        module: "",
        permission: "",
        visible: false
    },
    specialShedule: {
        module: "",
        permission: "",
        visible: false
    },
    moduleByRol: {
        module: "",
        permission: "",
        visible: false
    },
    user: {
        module: "",
        permission: "",
        visible: false
    }
};

