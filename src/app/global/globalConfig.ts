/*=================================================================================
                            MENSAJES DE FORMULARIOS
==================================================================================*/

export const MESSAGE_ALERT = {
    invalid_permissions: "Usted no tiene permisos para realizar esta acción, por favor valide con un administrador",
    add: "Datos Almacenados Correctamente",
    update:" Datos Actualizados Correctamente",
    duplicate: "Registro Previemente Realizado",
    error: "Error interno",
    mandatory: "Por favor revise la información"
};

/*=================================================================================
                            MENSAJES DE PERMISOS DE USUARIO
==================================================================================*/
export const PERMISSION_MESSAGES = {
    not_get_permissions: "No se obtuvieron permisos ",
    permissions_error: "Error obteniendo permisos"

};