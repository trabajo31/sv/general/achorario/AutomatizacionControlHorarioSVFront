import { Injectable } from '@angular/core';
import { FILTER_OPTIONS, FILTER_OPTIONS_PERMISSIONS, DAYS_OPTIONS } from '../../environments/environment';
import { NotificationsService } from '../shared/notifications/notificationsServices';

@Injectable({
  providedIn: 'root'
})
export class GlobalMethods {

  // Objeto con información de filtros. (Activo, Inactivo).
  filterOptions = FILTER_OPTIONS;
  days_options = DAYS_OPTIONS;
  filterOptionsPermissions = FILTER_OPTIONS_PERMISSIONS;
  constructor(private notificationsService: NotificationsService) { }

  /**
 * @date(22-02-2020)
 * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
 * @description Metodo global, encargado de optener el valor de la opcion de filtros, (Activo, Inactivo).
 **/
  validateFilterOptions(option: number) {
    switch (Number(option)) {
      case 0:
        return this.filterOptions[0].label;
      case 1:
        return this.filterOptions[1].label;
      default:
        return "";
    }
  }

  /**
* @date(22-02-2020)
* @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
* @description Metodo global, encargado de optener el valor de la opcion de filtros, (Activo, Inactivo).
**/
  validateFilterOptionsPermissions(option: number) {
    switch (option) {
      case 0:
        return this.filterOptionsPermissions[0].label;
      case 1:
        return this.filterOptionsPermissions[1].label;
      default:
        return "";
    }
  }

  /**
  * @date(29-09-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de validar si entran caracteres diferentes a numeros
  **/
  validateCharacterSize(label: string = 'Actual', textData: string, maxSize: number) {
    const size = textData.length;
    if (size >= maxSize) {
      this.notificationsService.alert('Aviso', 'El campo (' +
        label + ') No puede ser mayor a los (' + maxSize +
        ') caracteres.', 'warning');
    }
  }


  /**
   * @date(22-02-2020)
   * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
   * @description Metodo global, encargado de optener el valor de la opcion de filtros, (Activo, Inactivo).
   **/
  validateFilterDays(option: number) {
    switch (Number(option)) {
      case 1:
        return this.days_options[0].label;
      case 2:
        return this.days_options[1].label;
      case 3:
        return this.days_options[2].label;
      case 4:
        return this.days_options[3].label;
      case 5:
        return this.days_options[4].label;
      case 6:
        return this.days_options[5].label;
      case 7:
        return this.days_options[6].label;
      default:
        return "";
    }
  }



  /**
  * @date(29-09-2020)
  * @author Santiago Vélez Pulgarin <santiago.velez@grupokonecta.com>
  * @description Metodo encargado de validar la longitud de la cadena de texto.
  **/
  validateOnlyNumbers(data: any) {
    const k = data.charCode;
    return (k > 47 && k < 58);
  }

}