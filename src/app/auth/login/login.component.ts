import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UntypedFormControl, Validators, UntypedFormGroup } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import 'rxjs/add/operator/delay';

import { AuthenticationService } from '../../core/services/auth.service';
import { NotificationService } from '../../core/services/notification.service';
import { UserService } from 'src/app/services/user/user.service';
import { NotificationsService } from 'src/app/shared/notifications/notificationsServices';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    loginForm: UntypedFormGroup;
    loading: boolean;

    constructor(private router: Router,
        private titleService: Title,
        private notificationService: NotificationService,
        private userService: UserService,
        private authenticationService: AuthenticationService,
        private notificationsService: NotificationsService,) {
    }

    ngOnInit() {
        this.titleService.setTitle('Control de horarios - Iniciar Sesion');
        this.createForm();
        const token = localStorage.getItem("token");
        if(token != null){
            this.authenticationService.setCurrentUser(token); 
            this.router.navigate(['/customers']);
        }
    }

    logout() {
        this.authenticationService.logout();
    }

    private createForm() {

        this.loginForm = new UntypedFormGroup({
            username: new UntypedFormControl('', Validators.required),
            password: new UntypedFormControl('', [Validators.required , Validators.minLength(8)])
        });
    }

    login() {
        const username = this.loginForm.get('username').value;
        const password = this.loginForm.get('password').value;

        const loginData = new FormData();

        loginData.append('networkUser', username);
        loginData.append('password', password);


        this.loading = true;
        this.userService.signIn(loginData).subscribe(
                resp => {
                    if(resp.status && resp.data != null){
                        this.authenticationService.setCurrentUser(resp.data); 
                        this.router.navigate(['/customers']);
                    }else{
                        this.notificationsService.alert('Aviso', 'Las credenciales no son válidas', 'error');
                    }
                },
                error => {
                    this.loading = false;
                }
            );
            this.loading = false;
    }
}
