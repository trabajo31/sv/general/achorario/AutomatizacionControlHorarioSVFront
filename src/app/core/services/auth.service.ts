import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as jwt_decode from 'jwt-decode';
import * as moment from 'moment';
import 'rxjs/add/operator/delay';
import { of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    private user: any = null;
    constructor(private http: HttpClient,
        @Inject('LOCALSTORAGE') private localStorage: Storage) {
    }

    setCurrentUser(token: string) {
        token = token.replace('Bearer ','');
        this.localStorage.setItem("token", token);
        const decodedToken = jwt_decode(token);
        const decodedData = JSON.parse(decodedToken['sub']);
        var date = decodedToken["exp"];
        var isAdmin = false;
        if (decodedData["k_rol_user"]["descriptionRol"] == "ADMINISTRADOR"){
            isAdmin = true;
        }
        var currentUser = {
            token: token,
            isAdmin: isAdmin,
            email: 'konecta@grupokonecta.com',
            id: decodedData['number_id_user'],
            alias: decodedData["network_user"],
            expiration: date,
            fullName: decodedData["name_user"]
        }
        this.user = currentUser;
        return true;
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.user = null;
        localStorage.removeItem("token");
    }

    getCurrentUser(): any {
        if (this.user != null) {
            let newExpiration = moment(this.user['expiration']*1000)
            if (newExpiration.isValid()) {
                this.user['expiration'] = newExpiration;
            }

            return this.user;
        }
        return null;
    }

    passwordResetRequest(email: string) {
        return of(true).delay(1000);
    }

    changePassword(email: string, currentPwd: string, newPwd: string) {
        return of(true).delay(1000);
    }

    passwordReset(email: string, token: string, password: string, confirmPassword: string): any {
        return of(true).delay(1000);
    }
}
