import { Directive, ElementRef, Input, HostListener } from '@angular/core';

@Directive({
  selector: '[appOnlyLetters]'
})
export class OnlyLettersDirective {

  constructor(private el: ElementRef) { }

  @Input() onlyLetters: boolean;

  @HostListener('keydown', ['$event']) onKeyDown(event) {
    const e = <KeyboardEvent>event;
    if (this.onlyLetters) {
      if (['46', '8', '9', '27', '13', '32', '192'].indexOf(e.code) !== -1 ||
        // Allow: Ctrl+A
        (e.code === '65' && e.ctrlKey === true) ||
        // Allow: Ctrl+C
        (e.code === '67' && e.ctrlKey === true) ||
        // Allow: Ctrl+X
        (e.code === '88' && e.ctrlKey === true) ||
        // Allow: home, end, left, right
        (e.code >= '35' && e.code <= '39')) {
        // let it happen, don't do anything
        return;
      }

      // Ensure that it is a number and stop the keypress
      if ((e.code < '65' || e.code > '90')) {
        e.preventDefault();
      }
    }
  }

}
