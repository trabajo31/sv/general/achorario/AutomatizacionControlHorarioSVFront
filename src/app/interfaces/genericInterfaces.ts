/*****************************/
/* Type Alerts */
/*****************************/
export type Types = 'error' | 'info' | 'success' | 'warning' | 'question';

/*****************************/
/* Select Options */
/*****************************/
export interface Options {
    id: string,
    option: string
}