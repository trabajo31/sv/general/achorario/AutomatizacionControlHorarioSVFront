import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  URI: string;
  constructor(private http: HttpClient) {
    this.URI = `${environment.apiUrl}`;
  }
  getAllService(): Observable<any> {
    return this.http.get<any>(`${this.URI}user/list`);
  }
  signIn(data): Observable<any> {
    return this.http.post<any>(`${this.URI}user/login`, data);
  }
  save(data): Observable<any> {
    return this.http.post<any>(`${this.URI}user/save`, data);
  }

  update(data): Observable<any> {
    return this.http.put<any>(`${this.URI}user/update`, data);
  }
}
