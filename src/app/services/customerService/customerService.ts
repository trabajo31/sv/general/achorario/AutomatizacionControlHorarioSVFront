import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class CustomerService {

    url: string;

    constructor(private http: HttpClient) {
        this.url = `${environment.apiUrl}`
    }

    getAllDataCustomer():Observable<any>{
        return this.http.get<any>(`${this.url}client/list`);
    }

    getAllCustomersActive(): Observable<any> {
        return this.http.get<any>(`${this.url}client/listActive`);
      }



}