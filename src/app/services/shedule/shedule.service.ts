import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SheduleService {

  URI: string;
  constructor(private http: HttpClient) {
    this.URI = `${environment.apiUrl}`;
  }
  getAllService(): Observable<any> {
    return this.http.get<any>(`${this.URI}shedule/list`);
  }
  getServices(data): Observable<any> {
    return this.http.post<any>(`${this.URI}shedule/listService`, data);
  }
  getAllDataCustomer(): Observable<any> {
    return this.http.get<any>(`${this.URI}shedule/listCustomer`);
  }
  save(data): Observable<any> {
    return this.http.post<any>(`${this.URI}shedule/save`, data);
  }
  getAllDataCustomerAndService(data): Observable<any> {
    return this.http.post<any>(`${this.URI}shedule/listScheduleCustomerAndService`, data);
  }
  getInactive(data): Observable<any> {
    return this.http.post<any>(`${this.URI}shedule/delete`, data);
  }
  update(data): Observable<any> {
    return this.http.put<any>(`${this.URI}shedule/update`, data);
  }
  /**
   * @dete(30-08-2020)
   * @author Jesus David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
   * @description Metodo encargado de eliminar los datos.
  **/
  deleteShedule(data): Observable<any> {
    return this.http.post<any>(`${this.URI}shedule/deleteAll`, data);
  }



}
