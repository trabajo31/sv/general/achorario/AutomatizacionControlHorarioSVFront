
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RolService {
  URI: string;
  constructor(private http: HttpClient) {
    this.URI = `${environment.apiUrl}`;
  }
  getAllService(): Observable<any> {
    return this.http.get<any>(`${this.URI}roles/list`);
  }

  save(data): Observable<any> {
    return this.http.post<any>(`${this.URI}roles/save`, data);
  }

  update(data): Observable<any> {
    return this.http.put<any>(`${this.URI}roles/update`, data);
  }

}

