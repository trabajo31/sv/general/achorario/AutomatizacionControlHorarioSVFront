import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  URI: string;
  constructor(private http: HttpClient) {
    this.URI = `${environment.apiUrl}`;
  }
  getAllCustomers(): Observable<any> {
    return this.http.get<any>(`${this.URI}client/list`);
  }
  getAllCustomersActive(): Observable<any> {
    return this.http.get<any>(`${this.URI}client/listActive`);
  }

  save(data): Observable<any> {
    return this.http.post<any>(`${this.URI}client/save`, data);
  }

  update(data): Observable<any> {
    return this.http.put<any>(`${this.URI}client/update`, data);
  }

  delete(data): Observable<any> {
    return this.http.post<any>(`${this.URI}client/delete`, data);
  }
}
