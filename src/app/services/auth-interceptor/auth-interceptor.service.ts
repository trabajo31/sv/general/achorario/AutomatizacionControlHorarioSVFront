import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor(private router: Router, private authenticationService: AuthenticationService,) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const user = this.authenticationService.getCurrentUser();
    let token:string  = null;
    if (user) {
      token = user.token;
    }

    let request = req;

    if (token) {
      if (!token.includes("Bearer")) {
        token = "Bearer " + token;
      }
      request = req.clone({
        setHeaders: {
          Authorization: token
        }
      });
    }
    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse) => {
        if (err.status === 403 || err.status === 401) {
          this.router.navigate(['/auth']);
        }
        return throwError(err);
      })
    );
  }
}
