import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class SpecialScheduleService {

  URI: string;

  constructor(private http: HttpClient) {
    this.URI = `${environment.apiUrl}`
  }

  /**
   * @dete(30-08-2020)
   * @author Jesus David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
   * @description Metodo encargado de listar los datos
  **/
  getAllSpecialSchedule(): Observable<any> {
    return this.http.get<any>(`${this.URI}SpecialSchedule/list`);
  }
    /**
   * @dete(30-08-2020)
   * @author Jesus David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
   * @description Metodo encargado de guardar los datos.
  **/
  save(data): Observable<any> {
    return this.http.post(`${this.URI}SpecialSchedule/save`, data);
  }
  /**
   * @dete(30-08-2020)
   * @author Jesus David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
   * @description Metodo encargado de actualizar.
  **/
  update(data): Observable<any> {
    return this.http.put<any>(`${this.URI}SpecialSchedule/update`, data);
  }

  /**
   * @dete(30-08-2020)
   * @author Jesus David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
   * @description Metodo encargado de eliminar los datos.
  **/
 deleteSpecialShedule(data):Observable<any>{
  return this.http.post<any>(`${this.URI}SpecialSchedule/deleteAll`, data);
 }

}