import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HolydayByCountryService {
  URI: string;

  constructor(
    private http: HttpClient
  ) {
    this.URI = `${environment.apiUrl}`;
  }

  getAllHolydaysByCountry() {
    return this.http.get<any>(`${this.URI}holydayByCountry/list`);
  }

  save(data): Observable<any> {
    return this.http.post<any>(`${this.URI}holydayByCountry/save`, data);
  }

  update(data): Observable<any> {
    return this.http.put<any>(`${this.URI}holydayByCountry/update`, data);
  }
}
