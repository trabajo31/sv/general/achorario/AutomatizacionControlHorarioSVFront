import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {
  URI: string;

  constructor(private http: HttpClient) {
    this.URI = `${environment.apiUrl}`;
  }

  getAllCountries(): Observable<any> {
    return this.http.get(`${this.URI}countries/list`);
  }

  save(data): Observable<any> {
    return this.http.post<any>(`${this.URI}countries/save`, data);
  }
  update(data): Observable<any> {
    return this.http.put<any>(`${this.URI}countries/update`, data);
  }


  delete(data): Observable<any> {
    return this.http.post<any>(`${this.URI}countries/delete`, data);
  }
}
