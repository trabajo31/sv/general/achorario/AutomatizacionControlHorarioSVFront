import { Injectable } from '@angular/core';
import { Types } from '../../interfaces/genericInterfaces';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})

export class AlertService {

  constructor() { }

  /**
   * @date (30-08-2020)
   *@author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
   * @description Metodo encargado de mostrar el popup de confirmacion solo con las opciones SI y NO
   * @params { strTitle, strMessage, strType } cuerpo in encabezado del popup
   * @return { boolean } retorna si se acepto o cancelo la accion a ejecutar
  **/
  confirm(strTitle: string, strMessage: string, strType: Types, confirmButton = 'Si', cancelButton = 'No') {

    return new Promise<any>( resolve => {

      Swal.fire({
        title: strTitle,
        text: strMessage,
        /* type: strType, */
        confirmButtonText: confirmButton,
        cancelButtonText: cancelButton,
        showCancelButton: true,
      }).then((result) => {
        if (result.value) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });

  }


    /**
   * @date (30-08-2020)
   * @author Jesús David Sánchez Pabón <jesus.sanchez@grupokonecta.com>
   * @description Metodo encargado de mostrar el popup de confirmacion solo con la opciones ACEPTAR
   * @params  strTitle encabezado del popup
   * @params  strMessage cuerpo del popup
   * @params  strType tipo de mensaje
   * @return N/A
  **/

  confirmReading(strTitle: string, strMessage: string, strType: Types, confirmButton = 'Aceptar'){

    Swal.fire({
      title: strTitle,
      html: strMessage,
      /* type: strType, */
      confirmButtonText: confirmButton,
    });
  }

  warning(title: string, text: string) {
    Swal.fire({
        title: title,
        html: text,
        icon: 'warning',
        buttonsStyling: false,
        confirmButtonText: '<i class="material-icons">check</i> Aceptar',
        customClass: {
            confirmButton: "btn btn-sm btn-primary"
        }
    });
  }

}
