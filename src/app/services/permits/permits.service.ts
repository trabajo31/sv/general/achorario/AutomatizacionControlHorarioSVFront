import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PermitsService {

  URI: string;
  constructor(private http: HttpClient) {
    this.URI = `${environment.apiUrl}`
  }

  getPermits(idRol = ''): Observable<any> {
    let dataSave = { 'rol': idRol};
    // return this.http.get<any>(`${this.URI}modulebyrol/listByRol?rol=${idRol}`);
    return this.http.post<any>(`${this.URI}modulebyrol/listByRol`, dataSave);
  }

}
