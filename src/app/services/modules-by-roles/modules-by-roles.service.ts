import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModulesByRolesService {

  URI: string;

  constructor(private http: HttpClient) {
    this.URI = `${environment.apiUrl}`;
  }

  getAllModulesByService(): Observable<any> {
    return this.http.get<any>(`${this.URI}modulebyrol/list`);
  }

  getAllModules(): Observable<any> {
    return this.http.get<any>(`${this.URI}modules/list`);
  }

  getAllPermissions(): Observable<any> {
    return this.http.get<any>(`${this.URI}permissions/list`);
  }

  save(params): Observable<any> {
    return this.http.post<any>(`${this.URI}modulebyrol/save`, params);
  }
}
