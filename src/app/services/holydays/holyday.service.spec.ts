import { TestBed } from '@angular/core/testing';

import { HolydayService } from './holyday.service';

describe('HolydayService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HolydayService = TestBed.get(HolydayService);
    expect(service).toBeTruthy();
  });
});
