import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HolydayService {

  URI: string;
  constructor(private http: HttpClient) {
    this.URI = `${environment.apiUrl}`;
  }

  getAllService(): Observable<any> {
    return this.http.get<any>(`${this.URI}holydays/list`)
  }

  getAllHolyDays(): Observable<any> {
    return this.http.get<any>(`${this.URI}holydays/list`)
  }

  save(data): Observable<any> {
    return this.http.post<any>(`${this.URI}holydays/save`, data)
  }

  update(data): Observable<any> {
    return this.http.put<any>(`${this.URI}holydays/update`, data)
  }

  delete(data): Observable<any> {
    return this.http.post<any>(`${this.URI}holydays/delete`, data)
  }
}
