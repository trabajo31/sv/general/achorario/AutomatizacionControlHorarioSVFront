import { Directive } from '@angular/core';
import { 
  AbstractControl, NG_VALIDATORS, Validator, ValidationErrors, ValidatorFn
} from '@angular/forms';
import { ServicesModel } from '../../../model/services.model';

export function emailValidator(): ValidatorFn {
  return (control: AbstractControl) => {
    const passwordValidationDirective = new FormValidationsDirective();
    return passwordValidationDirective.validate(control);
  };
}

export function userValidator(): ValidatorFn {
  return (control: AbstractControl) => {
    const passwordValidationDirective = new FormValidationsDirective();
    return passwordValidationDirective.validateUser(control);
  };
}

export function virtualServiceValidator(mensaje): ValidatorFn {
  return (control: AbstractControl) => {
    const passwordValidationDirective = new FormValidationsDirective();
    return passwordValidationDirective.virtualServiceValidator(control, mensaje);
  };
}

export function userSvValidator(usersNameSV): ValidatorFn {
  return (control: AbstractControl) => {
    const passwordValidationDirective = new FormValidationsDirective();
    return passwordValidationDirective.userSvValidator(control, usersNameSV);
  };
}

export function validatePassword(): ValidatorFn {
  return (control: AbstractControl) => {
    const passwordValidationDirective = new FormValidationsDirective();
    return passwordValidationDirective.validatePassword(control);
  };
}

export function clientValidValidator(val): ValidatorFn {
  return (control: AbstractControl) => {
    const passwordValidationDirective = new FormValidationsDirective();
    return passwordValidationDirective.clientValidValidator(control, val);
  };
}

export function validateXSS(): ValidatorFn {
  return (control: AbstractControl) => {
    const passwordValidationDirective = new FormValidationsDirective();
    return passwordValidationDirective.validateXSS(control);
  };
}

export function urlStratrgy(): ValidatorFn {
  return (control: AbstractControl) => {
    const passwordValidationDirective = new FormValidationsDirective();
    return passwordValidationDirective.urlStratrgy(control);
  };
}

export function nameValidator(): ValidatorFn {
  return (control: AbstractControl) => {
    const passwordValidationDirective = new FormValidationsDirective();
    return passwordValidationDirective.validateName(control);
  };
}

export function validateNameService(clients: ServicesModel[], dataUpdate): ValidatorFn {
  return ((control: AbstractControl) => {
    const validateNameServiceDirective = new FormValidationsDirective();
    return validateNameServiceDirective.validateNameService(clients, dataUpdate, control);
  });
}

export function onlyLetterAndNumbers(): ValidatorFn {
  return (control: AbstractControl) => {
    const passwordValidationDirective = new FormValidationsDirective();
    return passwordValidationDirective.validateText(control);
  };
}

export function onlyNumbers(): ValidatorFn {
  return (control: AbstractControl) => {
    const passwordValidationDirective = new FormValidationsDirective();
    return passwordValidationDirective.validateNumberText(control);
  };
}

@Directive({
  selector: '[appFormValidations]',
  providers: [{provide: NG_VALIDATORS, useExisting: FormValidationsDirective, multi: true}]
})
export class FormValidationsDirective implements Validator {

  validate(control: AbstractControl): ValidationErrors | null {
    const email = control.value;
    const expression = /^[\pL\s.]+[@]grupokonecta.com/;
    if(!email) {
      return {'validacion': {'message': 'Este campo es obligatorio'}};
    } else {
        if (!expression.test(email)) {
          return {'validacion': {'message': 'El email debe ser dominio @grupokonecta.com'}};
        } 
      
    }
    return null;
  }

  validateUser(control: AbstractControl): ValidationErrors | null {
    const email = control.value;
    const expression = /^[a-zA-Z.áéíóúüàè\xF1\xD1]*$/;
    if(!email) {
      return;
    } else {
        if (!expression.test(email)) {
          return {'validacion': {'message': 'Ingrese un usuario valido'}};
        } 
    }
    return null;
  }

  virtualServiceValidator(control: AbstractControl, mensaje): ValidationErrors | null {
    const value = control.value;
    const expression = /^[a-zA-Z .áéíóúüàè\-_\xF1\xD1]*$/;
    let validate = null;
    if(!value) {
      validate = {'validacion': {'message': 'El ' + mensaje + ' es obligatorio'}};
    } else {
        if (!expression.test(value)) {
          validate = {'validacion': {'message': 'Ingrese un ' + mensaje + ' valido'}};
        } 
    }
    return validate;
  }

  userSvValidator(control: AbstractControl, usersNameSV): ValidationErrors | null {
    const value = control.value;
    let validate = null;
    usersNameSV.forEach(e => {
      if(value === e.service_user) {
        validate = { 
          'validacion': { 
            'message': 'Este usuario de autenticación ya está siendo utilizado en otro servicio'
          }
        };
      }
    });

    return validate;
  }

  urlStratrgy(control: AbstractControl): ValidationErrors | null {
    const value = control.value;
    /*
    * ¡IMPORTANTE!
    * INFORMACION AL EQUIPO DE SEGURIDAD: 
    * Esta es una validación de regla de negocio
    */
    const expression = /^https:\/\/widget.grupokonecta.co\/genesys\/2\/chat\/+[A-Za-z\-_\s\xF1\xD1]+$/;
    if(!value) {
      return {'validacion': {'message': 'La url de estrategia es obligatoria'}};
    } else {
        if (!expression.test(value)) {
          return {'validacion': {'message': 'La estrategia debe ser parecida a la siguiente url de ejemplo: https://widget.grupokonecta.co/genesys/2/chat/example  '}};
        } 
    }
    return null;
  }

  validatePassword(control: AbstractControl): ValidationErrors | null {
    const value = control.value;
    /*
    * ¡IMPORTANTE!
    * INFORMACION AL EQUIPO DE SEGURIDAD: 
    * Esta es una validación de regla de negocio
    */
    const expression = /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[^\w\d\s:])([^\s]){8,50}$/gm;
    if(!value) {
      return {
        'validacion': 
          {
            'message': 'La contraseña es obligatoria'
          }
        };
    } else {
        if (!expression.test(value)) {
          return {
            'validacion': 
              {
                'message': 'La contraseña debe tener entre 8 y 50 caracteres y contener una letra mayúscula, un símbolo y un número'
              }
            };
        } 
    }
    return null;
  }

  clientValidValidator(control: AbstractControl, clients): ValidationErrors | null {
    const value = control.value;
    let dataValidate = {'validacion': {'message': 'Debe seleccionar un rol existente'}};
    clients.forEach(element => {
      if(value === element) {
          dataValidate = null;
      }
    });
    return dataValidate;
  }

  validateXSS(control: AbstractControl): ValidationErrors | null {
    // const value = control.value;
    const inputText = control.value;

    const blackList = [
      '<script>', 'script','execCommand', 'backColor', 'createLink', 
      'delete', 'insertHTML', 'subscript', 'superscript', 'underline', 'unlink', 
      'textContent', 'sessionStorage', 'alert', 'javascript', "/drop(\s)/i", 
      "/insert(\s)/i", "/javascript/i", "/script/i", "/alert/i", "/select(\s)/i","/update(\s)/i", "/show(\s)/i", "/table(\s)/i",
      "/create(\s)/i","/--/","/union(\s)/i","/into(\s)/i","/\.\*/",
      "/((\"|')?[0-9](\"|')?(\s){0,})\=((\s){0,}(\"|')?[0-9](\"|')?(\s){0,})/"
    ];
    let dataValidate = null;
    blackList.forEach(element => {
      if(inputText.includes(element)) {
        dataValidate = {'validacion': {'message': 'El texto ingresado no es valido'}};
      }
    });
    const expression = /^(\s|\w|\d|\.|\,|\;|\:|\?|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\_|\~|\`|\'|\\|\-|\/|\+|ñ)*?$/;

    if(!inputText) {
      return {'validacion': {'message': 'Este campo es obligatorio'}};
    } else {
        if (!expression.test(inputText)) {
          return {'validacion': {'message': 'El texto ingresado no es valido'}};
        } 
    }

    return dataValidate;
  }



  validateName(control: AbstractControl): ValidationErrors | null {
    const name = control.value;
    const expression = /^[A-Za-z0-9 áéíóúüàè\s\xF1\xD1]+$/;
    if(!name) {
      return {'validacion': {'message': 'Este campo es obligatorio'}};
    } else {
        if (!expression.test(name)) {
          return {'validacion': {'message': 'Ingrese un nombre valido'}};
        } 
    }
    return null;
  }
  

  validateNameService(allServices: ServicesModel[], dataUpdate: ServicesModel, control: AbstractControl): ValidationErrors | null {
    let valid = true;

    if(!control.value) {
      return {'validacion': {'message': 'Este campo es obligatorio'}};
    } else {

      allServices.forEach(elemnt => {
        if(elemnt.name === control.value.toUpperCase()) {
          valid = false;
        }        
      });

      if(dataUpdate && dataUpdate.name === control.value.toUpperCase()) {
        valid = true;
      }

      if(!valid) {
        return {'validacion': {'message': 'Este servicio virtual ya se encuentra registrado'}};
      }
    }
    return null;
  }

  validateText(control: AbstractControl): ValidationErrors | null {
    const name = control.value;
    const expression = /^[A-Za-z 0-9áéíóúñüàè\s\xF1\xD1]+$/;
    if(!name) {
      return {'validacion': {'message': 'Este campo es obligatorio'}};
    } else {
        if (!expression.test(name)) {
          return {'validacion': {'message': 'Ingrese un texto valido'}};
        } 
    }
    return null;
  }

  validateNumberText(control: AbstractControl): ValidationErrors | null {
    const name = control.value;
    const expression = /^[0-9]+$/;
    if(!name) {
      return {'validacion': {'message': 'Este campo es obligatorio'}};
    } else {
        if (!expression.test(name)) {
          return {'validacion': {'message': 'Este campo solo valores numericos'}};
        } 
    }
    return null;
  }



}
