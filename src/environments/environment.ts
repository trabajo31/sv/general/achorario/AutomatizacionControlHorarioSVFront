import { NgxLoggerLevel } from 'ngx-logger';

//-------------------------------Constantes que se utilizaran en todo el sistema-------------------------------------//

//Constante encargada de devolver los valores de activo e inactivo
export const FILTER_OPTIONS = [
  {
    value: 0,
    label: "Inactivo"
  },
  {
    value: 1,
    label: "Activo"
  }
];

//Constante encargada de devolver los dias de la semana 
export const DAYS_OPTIONS = [
  {
    value: 1,
    label: "Lunes"
  },
  {
    value: 2,
    label: "Martes"
  },
  {
    value: 3,
    label: "Miércoles"
  },
  {
    value: 4,
    label: "Jueves"
  },
  {
    value: 5,
    label: "Viernes"
  },
  {
    value: 6,
    label: "Sábado"
  },
  {
    value: 7,
    label: "Domingo"
  }
  
];

//Constante encargada de devolver los valores de activo e inactivo
export const FILTER_OPTIONS_PERMISSIONS = [
  {
    value: "lectura",
    label: "Lectura"
  },
  {
    value: "lectura_escritura",
    label: "Lectura y Escritura"
  }
];

export const environment = {
  production: true,
  // apiUrl:'https://asistenciawebv2.grupokonecta.co:8443/ACH/api/',
  apiUrl:'http://localhost:8082/api/',
  logLevel: NgxLoggerLevel.TRACE,
  serverLogLevel: NgxLoggerLevel.OFF
};
